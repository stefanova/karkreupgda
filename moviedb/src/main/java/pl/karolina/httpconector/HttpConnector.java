package pl.karolina.httpconector;

import jdk.nashorn.internal.parser.JSONParser;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.Scanner;

public class HttpConnector {

  private URL obj;
  private HttpURLConnection con;
  private String userAgent;

  public  String sendGET(String url) throws IOException {

    obj = new URL(url);

    con = (HttpURLConnection) obj.openConnection();
    userAgent ="Karolina/1.0";

    con.setRequestMethod("GET");
    con.setRequestProperty("User-Agent",userAgent);

    int responseCode = con.getResponseCode();
    String result = "";
    if(responseCode == 200){

      //pobieranie danych poprzez streamowaniex

      InputStream res = con.getInputStream();
      InputStreamReader reader = new InputStreamReader(res);
      Scanner sc = new Scanner(reader);

      while(sc.hasNextLine()){
        result += sc.nextLine();
      } sc.close();

     // System.out.println(result);
    }
    return result;
  }

  public String sendPOST(String url, String params) throws IOException {

    obj= new URL(url);
    con = (HttpURLConnection) obj.openConnection();
    userAgent = "Pawel/2.0";

    con.setRequestMethod("POST");
    con.setRequestProperty("User-Agent", userAgent);
    con.setDoOutput(true);

    DataOutputStream dos = new DataOutputStream(con.getOutputStream());
    dos.writeBytes(params);
    dos.flush();
    dos.close();

//    koniec tylko dla POST

    Scanner sc = new Scanner(new InputStreamReader(con.getInputStream()));
    String lines ="";
    if(sc.hasNextLine()){
      lines += sc.nextLine();
    } sc.close();

    return lines;

    JSONParser jp = new JSONParser();
    JSONArray ja = null;
    JSONObject jo = null;
    try {
      Object o = jp.parse(lines);
      if(o instanceof JSONArray) {
        System.out.println("to jest array");
        ja = (JSONArray) o;
      } else if(o instanceof JSONObject) {
        System.out.println("to jest obiekt");
        jo = (JSONObject) o;
      }
    } catch (ParseException e) {
      e.printStackTrace();
    }

    if(jo != null) {

      System.out.println("status: " + jo.get("status"));
    }
  }

}


  var Translator = function() {
        this.pairs = {};

        this.addPair = function(pair) {
        if(pair instanceof Array) {
        this.pairs[ pair[0] ] = pair[1];
        } else if(pair instanceof Object) {
        var keys = Object.keys(pair);
        for(let elem of keys) {
        this.pairs[ elem ] = pair[ elem ];
        }
        }
        }

        this.showPairs = function() {
        var keys = Object.keys(this.pairs);
        for(let e of keys) {
        console.log(e + " = " + this.pairs[e]);
        }
        }

        this.translate = function(arg) {
        var keys = Object.keys(this.pairs);
        for(let i = 0; i < keys.length; i++) {
        arg = arg.split(keys[i]).join(this.pairs[keys[i]]);
        }
        return arg;
        }
        }

        var trans = new Translator();
        trans.addPair(['poszedł', 'went']);
        trans.addPair({ 'do': 'to'});
        trans.addPair({ 'lasu': 'forest', latać : 'fly' });
        console.log(trans.translate("Paweł poszedł do lasu."));






        var Dice = function() {
        this.number = [];
        this.numbersUnique = {};

//funkcja powinna losowac liczbe z zakresu 1 - 1-6
// i ja zwracac
        this.generateNumber = function() {
        return Math.floor((1 + Math.random() * 6) );
        }

//iterujemy sie od 0 do arg
// w kazdej iteracji wywolujemy metode
//this.generateNumber i jej wynik
// dodajemy do tablicy this.number
// nic nie zwracamy
        this.generateRandomNumbers = function( arg ) {
        for(var i = 0; i < arg; i++){
        this.number.push(this.generateNumber());
        }
        }

//iterujemy sie przez tablice this.number
// spr czy w obiekcie this.numbersUnique
//istnieje klucz zgodny z naszym rzutem kostka
//czyli 1..6
//jezeli tak - zwiekszamy jego wartosc
//jezeli nie, dodajemy go i ustawiamy jego wartosc
// na 1 (bo jest to pierwsze wystapienie)
        this.countNumbers = function() {
        for(let elem of this.number){
        if(this.numbersUnique[elem]){
        this.numbersUnique[elem]++
        } else {
        this.numbersUnique[elem] = 1;
        }

        }
        return this.numbersUnique;
        }
        this.init = function() {}
        }

        var dice = new Dice();
        dice.generateRandomNumbers(10);
        console.log(dice.number);
// console.log(dice.generateRandomNumbers(5));
        console.log(dice.countNumbers());