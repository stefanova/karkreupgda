package pl.karolina.moviedb.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.karolina.moviedb.entity.Genre;
import pl.karolina.moviedb.util.HibernateUtil;

import java.util.List;

public class GenreDao implements AbstractDao<Genre> {

  public boolean insert(Genre type) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    session.save(type);
    t.commit();
    session.close();
    return true;
  }

  public boolean delete(Genre type) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();

    if(this.get(type.getId()) == null ){
      return true;
    }
    session.delete(type);
    t.commit();
    session.close();
    return false;
  }

  public boolean delete(int id) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    session.delete(this.get(id));
    t.commit();
    return true;
  }

  public boolean update(Genre type) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    session.update(type);
    t.commit();
    session.close();
    return false;
  }

  public Genre get(int id) {
    Genre genre;
    Session session = HibernateUtil.openSession();
    genre = session.load(Genre.class, id);

    session.close();
    return genre;
  }

  public List<Genre> get() {

    List<Genre> genres;
    Session session = HibernateUtil.openSession();

    genres = session.createQuery("from Genre ").list();

    session.close();
    return genres;
  }


}
