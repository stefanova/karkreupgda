package pl.karolina.moviedb.dao;

import java.util.List;

public interface AbstractDao<T> {

  boolean insert(T type);
  boolean delete(T type);
  boolean delete(int id);
  boolean update (T type);
  T get (int id);
  List<T> get();
}
