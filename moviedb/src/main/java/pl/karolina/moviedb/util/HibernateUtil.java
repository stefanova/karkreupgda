package pl.karolina.moviedb.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

  private static final SessionFactory sessionFactory = buildSessionFactory();

  private static SessionFactory buildSessionFactory(){
    SessionFactory conf = null;

    try{
      conf = new Configuration().configure().buildSessionFactory();
    } catch (Exception e){
      System.out.println( e.getMessage());
    }
    return conf;

  }

  public static SessionFactory getSessionFactory() {return sessionFactory;}

  public static Session openSession(){ return getSessionFactory().openSession();}
}
