package pl.org.moviedb.servlet;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.org.moviedb.entity.Movie;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

@WebServlet(name = "MovieServlet")
public class MovieServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String action = request.getParameter("action");

    if(action.equals("add")){

        String name = request.getParameter("movieName");
        Session session = pl.org.moviedb.util.HibernateUtil.openSession();

        Movie m = new Movie(name);

        Transaction t = session.beginTransaction();
        session.save(m);
        t.commit();

    } else if(action.equals("edit")){
        String name = request.getParameter("movieName");
        int id = Integer.valueOf(request.getParameter("id"));
        Movie m = new Movie(name);
        m.setId(id);
        Session session = pl.org.moviedb.util.HibernateUtil.openSession();
        Transaction t = session.beginTransaction();
        session.update(m);
        t.commit();
        session.close();

    }
        request.getRequestDispatcher("MovieServlet?action=show").forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

            Session session = pl.org.moviedb.util.HibernateUtil.openSession();
            String action = request.getParameter("action");

            if(action.equals("show")){
                Transaction t = session.beginTransaction();
                List<Movie> movies = session.createQuery("from Movie where id > 0").list();
                t.commit();

                request.setAttribute("movies", movies);
                request.getRequestDispatcher("movies.jsp").forward(request,response);
            } else if(action.equals("edit")){

                int id = Integer.parseInt( request.getParameter("id"));
                Movie m = session.get(Movie.class, id);
                request.setAttribute("movie", m);
                request.getRequestDispatcher("edit.jsp").forward(request,response);


            }else if (action.equals("delete")){
                Transaction t = session.beginTransaction();
                int id = Integer.parseInt( request.getParameter("id"));

                Movie movie = new Movie();
                movie.setId(id);
                session.delete(movie);
                t.commit();
                request.getRequestDispatcher("MovieServlet?action=show").forward(request,response);

                } else {
                System.out.println( "Nie ma niczego");

            }

        session.close();

    }
}













//                public boolean delete(Animal type) {
//                    Session session = HibernateUtil.openSession();
//                    Transaction t = session.beginTransaction();
//                    if(this.get(type.getId()) == null){
//                        return true;
//                    }
//                    session.delete(type);
//                    t.commit();
//                    session.close();
//                    return false;