<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page isELIgnored="false" %>

<%--<c:import url="header.jsp"/>--%>

<%--&lt;%&ndash;<h3> Lista moich filmów </h3>&ndash;%&gt;--%>
    <%--<label for="movieName"> Lista moich filmów </label>--%>
    <%--<input type="hidden" name="action" value="get" id="movieName"/>--%>


<c:import url="header.jsp"/>


<section>

    <table border="1px">
        <thead>
        <tr>
            <th> ID </th>
            <th> Name </th>
            <th> Actions </th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items = "${movies}" var="movie">
        <tr>
        <td> <c:out value ="${movie.id}"/>. </td>
        <td> <c:out value ="${movie.name}"/> </td>
        <td> <a href="/MovieServlet?action=edit&id=<c:out value ="${movie.id}"/>"> Edit </a>
            &bull; <a href="/MovieServlet?action=delete&id=<c:out value ="${movie.id}"/>"> Delete</a> </td>
        </tr>
        </c:forEach>
        </tbody>
    </table>

</section>

<c:import url="footer.jsp"/>