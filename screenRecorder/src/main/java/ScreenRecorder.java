import java.util.ArrayList;
import java.util.List;

public class ScreenRecorder {

    private List<Profile> profileList = new ArrayList<Profile>();
    private Profile exampleProfile = null;
    private boolean isRecording = false;

    public void addProfile(Profile newProfile){
        profileList.add(newProfile);

    }

    public void setProfile(int index){

        if(index < 1 || index > profileList.size()){
            System.out.println("Profil nie istnieje");
        } else {
           exampleProfile = profileList.get(index+1);

        }
    }

    public void listProfiles(){
        for(int i = 0; i < profileList.size(); i++){
            System.out.println(((i+1)+ " : " + profileList.get(i)).toString());
        }

    }

    public void stopRecording(){

     if(isRecording == true){
         isRecording = false;
         System.out.println("Nagrywanie zostało wstrzymane.");
     }else{
         System.out.println("Nie ma czego zatrzymać. ");

    }}

    public void startRecording(){

        if (exampleProfile == null){
            System.out.println("Ustaw profil");
        } else if(isRecording == true){
            System.out.println("Nagrywanie w toku.");
        }else if( isRecording == false){
            isRecording = true;
            System.out.println("Nagrywanie profilu nr " + exampleProfile + " rozpoczęte.");
        }
    }

}
