import java.util.Scanner;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
//		findIndexInArray();
        ScreenRecorder recorder = new ScreenRecorder();

        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            String[] splits = line.split(" ");
            if(splits[0].equals("add")){
                String codecType = splits[1];
                Codec codec = Codec.valueOf(codecType);

                String extensionType = splits[2];
                Extension extension = Extension.valueOf(extensionType);

                String resolutionType = splits[3];
                Resolution resolution = Resolution.valueOf(resolutionType);

                Profile p = new Profile(codec, extension, resolution);
                recorder.addProfile(p);
            }else if(splits[0].equals("exit")){
                break;
            }else if(splits[0].equals("list")){
                recorder.listProfiles();
            }else if(splits[0].equals("start")){
                recorder.startRecording();
            }else if(splits[0].equals("stop")){
                recorder.stopRecording();
            }else if(splits[0].equals("setProfile")){
                Integer index = Integer.parseInt(splits[1]);
                recorder.setProfile(index);
            }
        }
    }
}
