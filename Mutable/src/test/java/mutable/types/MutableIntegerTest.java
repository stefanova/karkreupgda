package mutable.types;

import org.junit.Test;

import mutable.types.MutableInteger;

public class MutableIntegerTest {

	@Test
	public void test() {
		MutableInteger a = new MutableInteger(10);
		MutableInteger b = a;
		
		a.setValue(100);
		
		assert a.getValue() == 100;
		assert b.getValue() == 100;
		
		a.increment();

		assert a.getValue() == 101;
		assert b.getValue() == 101;
	}
}