import org.junit.Test;

public class OverloadingZadaniaTesty {
	
	@Test
	public void intMaxTest() {
		assert OverloadingZadania.max(1, 5) == 5;
	}
	
	@Test 
	public void doubleMaxTest(){
		assert OverloadingZadania.max(2.5, 8) == 8;
	}
	
	@Test 
	public void absFloat(){
		assert OverloadingZadania.abs(5f) == 5;
	}	

}
