
public class GeoMath {


		public static double squareArea(double a){
			return a*a;
		}
		
		public static double hexaArea( double a) {
			return 6 * squareArea(a);
		}
		
		public static double circlePerimieter (double r){
			
			return Math.PI * r * r;
		}
	
		public static double cylinder(double h, double r){
			return circlePerimieter(r) * h; 
		}
		
	

}
