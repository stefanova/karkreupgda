import org.junit.Test;

public class MetodyMatematykaTest {
	
	@Test		
		public void testIloczynu(){
			
			assert MetodyMatematyka.iloczyn(1,1) == 1; 
			assert MetodyMatematyka.iloczyn(1,10) == 10; 
			assert MetodyMatematyka.iloczyn(3,3) == 9; 


		}
	
	@Test
	public void testSzukaj(){
		
		int[] liczby = {1, 2, 3, 5, 3};
		assert MetodyMatematyka.szukaj(liczby,3) == 2;
		assert MetodyMatematyka.szukaj(liczby,4) == -1;
		assert MetodyMatematyka.szukaj(liczby,5) == 3;
		
	}
	
	@Test
	public void testMniejsza(){
		
		assert MetodyMatematyka.mniejsza(2,3) == 2;
		assert MetodyMatematyka.mniejsza(4,-7) == -7;
		assert MetodyMatematyka.mniejsza(0,100) == 0;
		
	}
	
	@Test
	public void testMniejsza2(){
		
		assert MetodyMatematyka.mniejsza(2,3,6) == 2;
		assert MetodyMatematyka.mniejsza(4,-7,4) == -7;
		assert MetodyMatematyka.mniejsza(0,100,4) == 0;
		
	}
		

}