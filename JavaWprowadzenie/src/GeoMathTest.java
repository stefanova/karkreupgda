import org.junit.Test;

public class GeoMathTest {

	@Test 
	public void squareAreaTest(){
		assert GeoMath.squareArea(2.0) == 4.0;
		assert GeoMath.squareArea(5.0) == 25.0;
//		assert GeoMath.squareArea(25.0) == 125.0;

	}
	
	@Test
	public void hexaAreaTest(){
		assert GeoMath.hexaArea(2.0) == 24.0;
		assert GeoMath.hexaArea(0.5) == 1.5;

	}
	
}
