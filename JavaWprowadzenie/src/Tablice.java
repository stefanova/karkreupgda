
public class Tablice {

	public static void main(String[] args) {
		int[] liczby = {2,3};
		
		System.out.println(liczby[1]);
		System.out.println(liczby[0]);
		
		int [] liczby100 = new int[100]; //Tablica stu zer
		System.out.println(liczby100[1]);
		
		boolean[] boole = new boolean[2];
		System.out.println(boole[0]);
		
		String[] slowa = new String[10];
		System.out.println(slowa[3]);
		
		char[] znaki = new char[3];
		System.out.println(znaki[1]);
		
		int [] exerciseOne = {1, 3, 5, 10};
		System.out.println(exerciseOne[0]);
		System.out.println(exerciseOne[1]);
		System.out.println(exerciseOne[2]);
		System.out.println(exerciseOne[3]);
		
		System.out.println(exerciseOne);
		
		System.out.println("@@@ Zawarto�� tablicy wydrukowana petla For:  ");
		
		for (int i = 0; i < exerciseOne.length; i++){
			System.out.println("Element na pozycji " + i + " " + exerciseOne[i]);
		}
		
		
		System.out.println("@@@ P�tla R�cznie od ty�u: ");
		for (int i = exerciseOne.length-1; i >= 0; i--){
			System.out.println("Element na pozycji " + i + " " + exerciseOne[i]);
		}
		
		System.out.println("@@@ P�tla ForEach: ");
		
		for (int liczba : exerciseOne) {
			System.out.println(liczba);
		}
		
		
	}

}
