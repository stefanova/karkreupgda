package reviewExercises;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class SimpleMethods {
	
	public static void main(String[] args) {
		
		
//		giveMeTheNumber();
//		showBiggerNumber();
//		giveMeYourName();
		
//		sum();
		
//		devide();
		
//		restFromDevide();
		
//		printHelloWorld();
		
		
		Random generator = new Random();
//		checkLetter();
//		arrayCopy();
//		arrayToList2();
//		arrayToList3();
		
		listToArray();

		
	}
	
	//Napisz program prosz�cy o liczb� i drukuj�cy napis �Liczba <podana liczba>�.
	
	public static int giveMeTheNumber(){
	Scanner sc = new Scanner(System.in);
	
	System.out.println("Podaj dowoln� liczb�: ");
	
	int number = sc.nextInt();
	
	System.out.println("Twoja liczba to : " + number);
	
	return number;
		
	}
	
	//Napisz program prosz�cy o dwie liczby a i b i sprawdzaj�cy, kt�ra jest wi�ksza.
	public static int showBiggerNumber(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj dwie dowolne liczby. Najpierw pierwsz�: ");
		
		int a = sc.nextInt();
		
		System.out.println("Teraz drug�: ");
		
		int b = sc.nextInt();
		
		if( !(a > b) && !(a == b)){
			
			System.out.println("Wi�ksza liczba to " + b);
			return b; 
		} else if ( a == b || b == a){
			System.out.println( "Liczby sa sobie r�wne " );
		} else {
			System.out.println("Wi�ksza liczba to " + a);
		}
		return a;
	}
	
	//Napisz program prosz�cy o imi� i wy�wietlaj�cy je.
	
	public static String giveMeYourName(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj swoje imi�: ");
		
		String name = sc.next();
		
		System.out.println("Cze�� " + name);
		
		return name;
	}
	
	//Napisz program prosz�cy o trzy liczby i wy�wietlaj�cy ich sum�.
	
	public static int sum(){
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj pierwsz� liczb�: ");
		int first = sc.nextInt();
		
		System.out.println("Podaj drug� liczb�: ");
		int second = sc.nextInt();
		
		int result = first + second;
		
		System.out.println( "Suma liczby " + first + " oraz liczby " + second + " wynosi " + result );
		
		return result;
		

	}
	
	//Napisz program prosz�cy o dwie liczby (typu double) i wy�wietlaj�cy wynik dzielenia.
	
	public static double devide(){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj dwie liczby, podzielimy je :D ");
		double first = sc.nextDouble();
		double second = sc.nextDouble();
		double result = first / second;
		
		System.out.println(first + " / " + second + " = " + result );
		
		return result;
	}
	
	//Napisz program prosz�cy o dwie liczby (typu int) i drukuj�ce reszt� z dzielenia jednej przez drug�.
	
	public static int restFromDevide(){
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Poda dwie liczby: ");
		int first = sc.nextInt();
		int second = sc.nextInt();
		int result = first % second; 
		System.out.println("Reszta z dzielenia "+ first + " i " + second + " wynosi: " + result );
		
		return result; 
	}

	
		//Program wy�wietlaj�cy �Hello World!� podan� przez u�ytkownika ilo�� razy.
	
	public static String printHelloWorld(){
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ile razy chcia�by� zobaczy� napis 'Hello World'?");
		String hello = "Hello World";
		int times = sc.nextInt();
		for(int i = 0; i < times; i++){
			System.out.println(hello);
		}
		
		return hello; 
	}
	
	//�rednie
	// 1. Program licz�cy ilo�� wyst�pie� danej liczby w tablicy.
	
//	public static int howManyTimes(){
//		int[] tablica = {1, 2, 2, 2, 3, 4, 4, 4, 5, 6, 6, 7}; 
//		int b;
//		int a = 0;
//		
//		for(int i = 0; i < tablica.length; i++){
//			 
//			if( b == tablica[i]){
//				a ++;
//			} else {
//				a = a;
//			}
//			
//		}
//		return a;
//	}

	//check if letter appears in word
	public static int checkLetter(){
		
		String s = "blablabla";
		int counter = 0; 
		
		for(int i = 0; i < s.length(); i++){
			if(s.charAt(i) == 'b'){
				counter ++; 
			}
			System.out.println(counter);
		} 
	return counter;
	}

	
	public static int[] arrayCopy(){
		int[] array = {1, 3, 3, 4, 5, 5};
		
		int[] array2 = array.clone();
		
				
		for(int i = 0; i < array2.length; i++){
			System.out.println((array2[i]) + 2);
		}
				
		return array2;
		
	}
	
	//Metoda przepisuj�ca zawarto�� tablicy do listy.
	
	public static int[] arrayToList(){
		
		Integer[] array = {1, 2, 3, 3, 4, 4};
		List<Integer> list = Arrays.asList(array);
		for(int element : list){
			System.out.println(element);
		}
		return null;
	}
	
	
	
	public static int[] arrayToList2(){
		Integer[] tablica = {2, 4, 6, 8, 10};
		List<Integer> lista = Arrays.asList(tablica);
		
		for( int element : lista){
			System.out.println(element);
		} 
		return null;
	}
	

	public static int[] arrayToList3(){
		Integer[] array = {2, 4, 6, 8, 10};
		List<Integer> list = Arrays.asList(array);
		for(int el : list){
			el += 1;
			System.out.println(el);
		}
		
		return null;
	}

	//Metoda przepisuj�ca zawarto�� listy do tablicy.

	public static String[] listToArray(){
		List<String> lista = new ArrayList<>();
		
		lista.add("Marysia");
		lista.add("Stasia");
		lista.add("Kasia");
		lista.add("Gabrysia");

		
		String [] tablica = new String[lista.size()];
		tablica = lista.toArray(tablica);
		
		for( String el : tablica){
			System.out.println(el);
		}
		
		System.out.println(tablica);
		
		return null;
	} 
	
        }
        

		
		
	

