public class SortBySelection {

  public static void sortBySelection(int[] array){
    for(int i = 0; i <array.length; i++){
      int min = array[i];
      int minIndex = i;

      for(int j = i + 1; j < array.length; j++){
         if(array[j] < min){
           min = array[j];
           minIndex = j;
        }
      }
      int buffer = array[i];
      array[i] = min;
      array[minIndex] = buffer;

    }
  }


}
