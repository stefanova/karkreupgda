
public class Sort {

    public static int[] sort(int[] array){

        int current;
        int next;
        boolean swapped = true;
        int size = array.length;

        while(swapped){
            swapped = false;
            for(int i = 0; i <size; i++){
                current = array[i];
                next = array[i+1];
                if(current > next){
                    array[i] = next;
                    array[i+1] = current;
                    size --;
                    swapped = true;
                }

                System.out.println(i);
            }
        }

        return array;
    }

//    public static void bubbleSort(int[] array){
//        for(int i = 0; i < array.length; i++){
//            for(int j = array.length -1 ; j > i ; j--){
//                int left = array[j-1];
//                int right = array[j];
//                if(left > right);
//                array[j-1] = right;
//                array[j] = left;
//            }
//
//        }
//    }
}
