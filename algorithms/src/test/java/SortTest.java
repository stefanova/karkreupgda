import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.junit.Assert.*;

public class SortTest {

  @Test
  public void sort() throws Exception {
    int[] array = {5, 1, 7, 8, 14, 2};
    Sort.sort(array);
    Assertions.assertThat(array).isSorted();
  }

  @Test
  public void sortByInsert() throws Exception {
    int[] array = {5, 1, 7, 8, 14, 2};
    SortByInsert.sortByInsert(array);
    Assertions.assertThat(array).isSorted();
  }

  @Test
  public void sortByPut() throws Exception {
    int[] array = {5, 1, 7, 8, 14, 2};
    SortByInsert.sortByPut(array);
    Assertions.assertThat(array).isSorted();
  }

  @Test
  public void sortBySelection() throws Exception {
    int[] array = {5, 1, 7, 2};
    SortBySelection.sortBySelection(array);
    Assertions.assertThat(array).isSorted();
  }

//  @Test
//  public void sortByInsert() throws Exception {
//    int[] array = {5, 1, 7, 8, 14, 2};
//    Sort.sortByInsert(array);
//    Assertions.assertThat(array).isSorted();
//  }

//  interface SortMethod{
//    void sort(int[] data);
//  }
//  public void testSort (SortMethod sortMethod){
//    int[] array = {1, 7, 8, 14, 2, 1,  4, 3, 8 ,9, 6};
//    sortMethod.sort(array);
//    Assertions.assertThat(array).isSorted();
//  }
//  @Test
//  public void sort() throws Exception{
//    testSort(Sort::sort);

//
//  @Test
//  public void sortByInsert() throws Exception{
//    testSort(SortByInsert::sortByInsert);
//  }

}
