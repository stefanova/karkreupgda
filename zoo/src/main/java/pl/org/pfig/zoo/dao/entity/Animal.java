package pl.org.pfig.zoo.dao.entity;


import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Animal {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name="id")
  private int id;
  @Column(name="name")
  private String name;

  public List<AnimalFeed> getAnimalFeedSet() {
    return animalFeedSet;
  }

  public void setAnimalFeedSet(List<AnimalFeed> animalFeedSet) {
    this.animalFeedSet = animalFeedSet;
  }

  @OneToMany(fetch = FetchType.LAZY)
  private List<AnimalFeed> animalFeedSet = new LinkedList<AnimalFeed>();


  public Animal() {
  }

  public Animal(String name) {
    this.name = name;
  }

  public Animal(int id, String name) {
    this.id = id;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }
}


