package pl.org.pfig.zoo.dao.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.org.pfig.zoo.dao.entity.Animal;
import pl.org.pfig.zoo.dao.entity.AnimalFeed;
import pl.org.pfig.zoo.dao.util.HibernateUtil;

import java.util.List;

public class AnimalFeedDao implements AbstractDao<AnimalFeed> {

  public boolean insert(AnimalFeed type) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    session.save(type);
    t.commit();
    session.close();
    return true;
  }

  public boolean delete(AnimalFeed type) {
    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    if(this.get(type.getId()) == null){
      return true;
    }
    session.delete(type);
    t.commit();
    session.close();
    return false;
  }

  public boolean delete(int id) {
    Session session = HibernateUtil.openSession();

    Transaction t = session.beginTransaction();

    session.delete(this.get(id));

    t.commit();
    return true;
  }

  public boolean update(AnimalFeed type) {
    Session session = HibernateUtil.openSession();

    Transaction t = session.beginTransaction();
    session.update(type);
    t.commit();
    session.close();
    return false;
  }

  public AnimalFeed get(int id) {

    AnimalFeed animalFeed;
    Session session = HibernateUtil.openSession();
    animalFeed = session.load(AnimalFeed.class, id);

    session.close();
    return animalFeed;
  }

  public List<AnimalFeed> get() {
    List<AnimalFeed> animalsFeed;
    Session session = HibernateUtil.openSession();

    animalsFeed = session.createQuery("from AnimalFeed").list();

    session.close();

    return animalsFeed;
  }

}
