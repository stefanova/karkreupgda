package pl.org.pfig.zoo.dao.entity;

import javax.persistence.*;

@Entity

public class AnimalFeed {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "id")
  private int id;

  @Column
  private String who;

  @Column
  private int amount;


  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  private Animal animal;

  public AnimalFeed(String who, int amount, Animal animal){
    this.who = who;
    this.amount = amount;
    this.animal = animal;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getWho() {
    return who;
  }

  public void setWho(String who) {
    this.who = who;
  }

  public int getAmount() {
    return amount;
  }

  public void setAmount(int amount) {
    this.amount = amount;
  }

  public Animal getAnimal() {
    return animal;
  }

  public void setAnimal(Animal animal) {
    this.animal = animal;
  }
}


