package pl.org.pfig.zoo.dao;

import pl.org.pfig.zoo.dao.dao.AnimalDao;
;
import pl.org.pfig.zoo.dao.entity.Animal;
import pl.org.pfig.zoo.dao.entity.AnimalFeed;

public class Main {
  public static void main(String[] args) {
    AnimalDao animalDao = new AnimalDao();

    // BEGIN: insert
    Animal a = new Animal();
    a.setName("Słoń");

    AnimalFeed animalFeedDao = new AnimalFeed("Paweł", 8, a);


//    animalDao.insert(a);
//    animalDao.insert(new Animal("Orzel"));
//    animalDao.insert(new Animal ("Gołąb"));
//
//    // BEGIN: retrieve
//
//    Animal slon = new Animal();
//    slon = animalDao.get(1);
//
//    System.out.println("Pobrales id=" + slon.getId() + " " + slon.getName());
//
//
//    // BEGIN: update
//    slon.setName("PrzerobionySlon");
//    animalDao.update(slon);
//
//    // BEGIN: delete
//    animalDao.delete(3); //delete:golab
//
//    Animal secondAnimal = new Animal();
//    secondAnimal.setId(2);
//    animalDao.delete(secondAnimal);
//
//    // BEGIN: retrieve all
//
//    for(Animal anim : animalDao.get()){
//      System.out.println( anim.getId() + ". " + anim.getName());
//    }
//  }
}