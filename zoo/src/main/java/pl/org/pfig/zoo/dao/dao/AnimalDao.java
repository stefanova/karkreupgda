package pl.org.pfig.zoo.dao.dao;

import pl.org.pfig.zoo.dao.entity.Animal;
import org.hibernate.Session;
import org.hibernate.Transaction;
import pl.org.pfig.zoo.dao.util.HibernateUtil;

import java.util.List;

public class AnimalDao implements AbstractDao<Animal> {
  public boolean insert(Animal type) {

    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    session.save(type);
    t.commit();
    session.close();
    return true;
  }

  public boolean delete(Animal type) {
    Session session = HibernateUtil.openSession();
    Transaction t = session.beginTransaction();
    if(this.get(type.getId()) == null){
      return true;
    }
    session.delete(type);
    t.commit();
    session.close();
    return false;
  }

  public boolean delete(int id) {
    Session session = HibernateUtil.openSession();

    Transaction t = session.beginTransaction();

    session.delete(this.get(id));

    t.commit();
    return true;
  }

  public boolean update(Animal type) {
    Session session = HibernateUtil.openSession();

    Transaction t = session.beginTransaction();
    session.update(type);
    t.commit();
    session.close();
    return false;
  }

  public Animal get(int id) {

    Animal animal;
    Session session = HibernateUtil.openSession();
    animal = session.load(Animal.class, id);

    session.close();
    return animal;
  }

  public List<Animal> get() {
    List<Animal> animals;
    Session session = HibernateUtil.openSession();

    animals = session.createQuery("from Animal").list();

    session.close();

    return animals;
  }
}
