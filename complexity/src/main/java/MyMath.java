import java.util.Random;

public class MyMath {

  public static int sum(int a, int b) { // O(1)n
    return a + b;
  }

  public static long pow(int a, int b) { // O(n)
    long result = 1;
    for (int i = 0; i < b; i++) {
      result *= a;
    }
    return result;
  }

  public static int[] randArray(int size) { // O()
    int[] result = new int[size];
    Random random = new Random();
    for (int i = 0; i < size; i++) {
      result[i] = random.nextInt();
    }
    return result;
  }

  public static int[][] randArrayTwoDim(int size) { // O(n^2)
    int[][] result = new int[size][];
    for (int i = 0; i < size; i++) {
      result[i] = randArray(size);
    }
    return result;
  }

  public static int find(int[] data, int value) { // O(n)
    for (int i = 0; i < data.length; i++) {
      if (data[i] == value) {
        return i;
      }
    }
    return -1;
  }

  public static int findBisection(int[] data, int value) { // log(n)
    int a = 0;
    int b = data.length - 1;
    while (a < b) {
      int s = (a + b) / 2;
      if (value == data[s]) {
        return s;
      } else if (value < data[s]) {
        b = s - 1;
      } else {
        a = s + 1;
      }
    }
    if (data[a] == value) {
      return a;
    }
    return -1;
  }
    }


