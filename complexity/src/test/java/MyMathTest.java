import org.junit.Test;

import static org.junit.Assert.*;

public class MyMathTest {

  @Test
  public void sum() throws Exception {
    assert MyMath.sum(4,5) == 9;
  }

  @Test
  public void pow() throws Exception {

    assert MyMath.pow(2, 3) == 8;
  }



}