package kolekcje;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class ListMathTest {
	
	@Test
	public void sumTest(){
		
		List<Integer> numbers = new ArrayList<Integer>();
		numbers.add(1);
		numbers.add(2);
		numbers.add(3);

		
		assert ListMath.sum(numbers) == 6;
		assert ListMath.multi(numbers) == 6;
		assert ListMath.average(numbers) == 2;
	}

}
