package kolekcje;

import java.util.Arrays;
import java.util.List;

public class CompareArrayToList {
	
			

	public static void main(String[] args) {
		

		int[] array = { 4, 2, 2, 1, 5, 29, 3, 8 };

		List<Integer> list = Arrays.asList(1, 2, 4, 2, 5, 12, 3, 2);
		
		System.err.println(countDuplicates(array, list));

	}

	public static int countDuplicates(int[] array, List<Integer> list) {
		int count = 0;
//		if(array.length != list.size()){
//			System.out.println(" Tablice nie s� sobie r�wne. ");
//			return 0;
//		}
		
		int elements = Math.min(array.length, list.size());
		for(int i = 0; i < elements; i++){
		if (array[i] == list.get(i)){
			count++;
		}		
	}
		return count;
}
}

// Napisz program tworz�cy dziesi�cioelementow� list� i tablice liczb,
// a nast�pnie sprawdzaj�cy ilu pozycjach warto�ci s� identyczne.
// Przyk�ad:
// Lista {1,2,4,2,5,12,3,2}
// Tablica {4,2,2,1,5,29,3,8}
// Wynik: 3
