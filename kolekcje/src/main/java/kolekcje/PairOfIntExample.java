package kolekcje;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class PairOfIntExample {

	public static void main(String[] args) {

		Set<PairOfInit> pairs = new HashSet<>();

		pairs.add(new PairOfInit(1, 2));
		pairs.add(new PairOfInit(2, 1));
		pairs.add(new PairOfInit(1, 1));
		pairs.add(new PairOfInit(1, 2));

		for(PairOfInit pairOfInit : pairs){
			System.out.println(pairOfInit);		
			}

	}

	// Utw�rz klas� ParaLiczb (int a,int b) i dodaj kilka instancji do zbioru:
	// {(1,2), (2,1), (1,1), (1,2)}.
	// Wy�wietl wszystkie elementy zbioru na ekran. Czy program dzia�a zgodnie z
	// oczekiwaniem?

}
