package kolekcje;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetsExample {

	public static void main(String[] args) {

		int[] tab = {6,2,1233,3,1,22,3,255,4,5555};
		
		//Zadanie zbiory 1, 
		//Rozwiazanie 2. 
		
//		Set<Integer> set = new HashSet<>(Arrays.asList(1,2,1,3,4,22,3,2,4,5));
		
		//Rozwiązanie 2. 
		
		Set<Integer> set = new TreeSet<>();	
//		Set<Integer> set = new HashSet<>();	
		
		
		
		for( int i = 0; i < tab.length; i++){
			set.add(tab[i]);
		}
		
		System.out.println(set);
		
		set.remove(22);
		set.remove(1);
		
		
		System.out.println(set);
	}
	


}
