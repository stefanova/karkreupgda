package kolekcje.mapy;

import java.util.HashMap;
import java.util.Map;

public class MapsExamples {
	
	public static void main(String[] args) {
		
		Map<String, String> slownik = new HashMap<>();
		slownik.put("Kot", "Cat");
		slownik.put("Pies", "Dog");
		
		
		System.out.println("Klucze: ");
		
		for ( String key : slownik.keySet()){
			System.out.println(key);
		}
		
		System.out.println("Wartości: ");
		for(String value : slownik.values()){
			System.out.println(value);
		}
		
		System.out.println("Pary klucz wartosc: ");
		
		//w jednym typie napisany inny typ --> klasa wewnetrzna
		

		for(Map.Entry<String, String> pair : slownik.entrySet()){
			System.out.println(pair);
		}
		
		slownik.remove("Pies");
		System.out.println("Pary klucz wartosc po usunieciu psa: ");
		for(Map.Entry<String, String> pair : slownik.entrySet()){
			System.out.println(pair);
		}
		System.out.println(slownik.get("Pies"));
		
		System.out.println(slownik.getOrDefault("Pies", "Brak tłumaczenia"));
		
	}

}
