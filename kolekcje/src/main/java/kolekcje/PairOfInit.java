package kolekcje;

public class PairOfInit {

	private final int first;
	private final int second;
	
	
	public PairOfInit(int first, int second) {
		this.first = first;
		this.second = second;
	}


	public int getFirst() {
		return first;
	}


	public int getSecond() {
		return second;
	}
	
	
@Override

public String toString(){
	return "(" + first +"," + second + ")";
}


@Override
public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + first;
	result = prime * result + second;
	return result;
}


@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	PairOfInit other = (PairOfInit) obj;
	if (first != other.first)
		return false;
	if (second != other.second)
		return false;
	return true;
}



}
