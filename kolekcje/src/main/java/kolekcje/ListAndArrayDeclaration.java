package kolekcje;

import java.util.ArrayList;
import java.util.Arrays;

import java.util.List;

public class ListAndArrayDeclaration {

	public static void main(String[] args) {

		int [] tabl = {1,2,3,4,5};
		
		int[] tabl2 = null;
		
		tabl2 = new int[]{1,2};
		
		tabl2 = new int[2];
		
		tabl2[0] = 1;
		tabl2[1] = 2;
		
		int [][] tab2dmi1 = {{1,2}, {3}, {2,3,5}};
		
		
		//Lista nieedytowalna
		List<Integer> lista = (List) Arrays.asList(1,2,3);
		
		//Mozna uczynic ja edytowalna
		
		List<Integer> lista2 = new ArrayList<>(Arrays.asList(1,2,3));
		
		//Tradycyjnie
		
//		List<Integer> lista2 = new ArrayList<>();
//		lista2.add(1);
//		lista2.add(2);

		
	}

}
