package kolekcje;

import org.junit.Test;



public class StringDuplicatesTest {			

	@Test
	public void containDuplicatesTest(){
		assert StringDuplicates.containDuplicates("abcdd") == false; 
		assert StringDuplicates.containDuplicates("abad") == true;
		
	}
	

}
