/* Pobierz wszystkie wiersze (select). */
SELECT * FROM contact;

/* Dodanie nowego rekordu danych z kolumny */
INSERT INTO contact (email) VALUES ("asdjsa@wp.pl");

/* Aktualizacja danych z kolumny, uzupelnienie pola NULL */
UPDATE contact SET email = 'asdjsa@wp.pl';

/*Pobierz tylko nazwiska i numery.*/
Select last_name, number from contact; 

/* Pobierz pary 
{ (concat(first_name," ",last_name) as names, e-mail}.
*/
SELECT concat(first_name, " ", last_name) as names, (email) as e_mail FROM contact; 

/* Pobierz ilość osób w tabeli. */

SELECT count(*) from contact; 

/* Ustaw we wszystkim kontaktach w których 
brakuje maila wartość “brak”.*/
Select email from contact; 

/* UPDATE TYLKO WYBRANYCH REKORDOW*/
UPDATE contact set email = NULL WHERE id < 4;
UPDATE contact set email = "Brak" WHERE email IS NULL;

/* Pobierz kontakty o konkretnym nazwisku */

SELECT last_name from contact WHERE last_name = 'Nowak'; 
/*  Pobierz kontakty o numerze zaczynającym się od cyfr 789*/

SELECT number from contact WHERE number LIKE '%789';

SELECT * from contact;

CREATE DATABASE company; 
CREATE TABLE worker(
id int AUTO_INCREMENT PRIMARY KEY,
name VARCHAR(12),
salary INT,
age INT
); 


/* ___________________________          WORKER           ___________________________________________ */

INSERT INTO worker (name, salary, age) VALUES 
("Kowalski",1000, 22),
("Bułka",1000, 44),
("Żabowska",1000, 33),
("Kowalik",1500, 34),
("Żaba",1500, 28),
("Bocian",1500, 27),
("Rolnik",2000, 59),
("Kowal",2000, 62),
("Radomska",4000, 36),
("Kowalska",4000, 49);

/*Pobierz wszystkie wiersze (select). */
SELECT * from worker order by id desc; 

/* Pobierz maksymalną, minimalną, średnią pensję. */
SELECT MAX(salary), MIN(salary), AVG(salary) from worker; 

/* Pobierz pary {workers_count, salary} */
Select count(*), salary from worker group by salary;

/* Pobierz pary {avg_salary, age} */
Select avg(salary), age from worker group by age;

/* Pobierz pracownika o najniższej pensji */
SELECT id, salary from worker,  (select MIN(salary), id from worker); 
SELECT * FROM worker where salary = (Select max(salary) from worker);
 

/*Pobierz pracownika o najwyższej pensji */
SELECT * FROM worker order by salary limit 1; 

/*Posortuj z odwrotna oklejnoscia */
SELECT * from worker order by id desc; 

/*Zmiana DATATYPE w kolumnie */
alter table worker modify column salary DECIMAL(8,2);
