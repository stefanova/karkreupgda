package pl.org.pfig.ug;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Random;
import java.util.Scanner;

public class UserGenerator {
	
	private final String path = "resources/";
	
	public User getRandomUser(){
		UserSex us = UserSex.SEX_MALE;
		if(new Random().nextInt(2) == 0){
			us = UserSex.SEX_FEMALE;
		}
		return getRandomUser(us); 
	}
	
	public User getRandomUser(UserSex sex ){
		User currentUser = new User();
		currentUser.setSex(sex);
		String name = "";			
				
		if(sex.equals(UserSex.SEX_MALE)){
			name = getRandomLineFromFile("name_m.txt");
		} else {
			name = getRandomLineFromFile("name_f.txt");
		}
				
		currentUser.setName(name);
		currentUser.setSecondName(getRandomLineFromFile("lastname.txt"));
		currentUser.setAddress(getRandomAddress());
		currentUser.setPhone(getRandomPhone());
		currentUser.setCCN(getRandomCcn());
		
		
		String dt = getRandomBirthDate(); // dd.MM.yyyy
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		try {
			currentUser.setBirthDate(sdf.parse(dt));
		} catch (ParseException e) {
			System.out.println(e.getMessage());
		}
		
		currentUser.setPESEL(getRandomPesel(currentUser));	
		return currentUser;
	}
	
	private String getRandomBirthDate() {
		int[] daysInMonths = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		int year = 1890 + new Random().nextInt(120);
		int month = new Random().nextInt(12) + 1;
		if(year % 4 == 0) {
			 daysInMonths[1]++;
		}
		int day = new Random().nextInt(daysInMonths[month - 1]) + 1;
		return leadZero(day) + "." + leadZero(month) + "." + year; // dd.MM.YYYY;
	}
	
	private String leadZero(int arg) {
		if(arg < 10) return "0" + arg;
		else return "" + arg;
	}
	
	public String getRandomPhone(){
		
		Random rand = new Random();
        int num1 = (rand.nextInt(7) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(1000);
        
        DecimalFormat df3 = new DecimalFormat("000");   
        String phone = df3.format(num1) + "-" + df3.format(num2) + "-" + df3.format(num3);

				
		return phone;
	}
	
	public String getRandomCcn(){
		
		Random rand = new Random();
	    int num1 = (rand.nextInt(8) + 1) * 1000 + (rand.nextInt(4) + 1) * 100 + (rand.nextInt(8) * 10) + rand.nextInt(8);
        int num2 = rand.nextInt(743);
        int num3 = rand.nextInt(1000);
        int num4 = rand.nextInt(1000);
		
        DecimalFormat df4 = new DecimalFormat("0000");   
        String ccn = df4.format(num1) + " " + df4.format(num2) + " " + df4.format(num3) + " " + (df4.format(num4));
		
        return ccn;
	}
	
	
	private Address getRandomAddress(){
		
		Address currentAddress = new Address();
		String[] cityData = getRandomLineFromFile("city.txt").split(" \t ");
		currentAddress.setCountry("Poland");
		currentAddress.setCity(cityData[0]);
		currentAddress.setZipcode(cityData[1]);
		currentAddress.setStreet("ul. " + getRandomLineFromFile("street.txt"));
		currentAddress.setNumber("" + (new Random().nextInt(100) + 1));
		
		return currentAddress; 
	}
	
	public String getRandomPesel(User u){
		
		// 870926 028 6 7
		// dataUrodzenia
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
		
		String[] birthDate = sdf.format(u.getBirthDate()).split("\\.");
		System.out.println(birthDate[1]);
		
		// cyfra kontrolna

		
		
//		1×8 + 3×7 + 7×0 + 9×9 + 1×1 + 3×6 + 7×0 + 9×2 + 1×8 + 3×6 + 1×7;
		
		
		
		return null;
		
	}
	
	private int countLines(String filename){
		
		int lines = 1;
		try(Scanner sc = new Scanner(new File(path + filename))){
			while(sc.hasNextLine()){
				lines++;
				sc.nextLine();
			}
		}catch (FileNotFoundException e){
			System.out.println(e.getMessage());
		}
		
		return lines;
	}
	
	private String getRandomLineFromFile(String filename){
		
		int allLines = countLines(filename);
		int line = new Random().nextInt(allLines) +1;
		int currentLine = 1; 
		String currentLineContent = ""; //aktualna wartosc aktualnej linijki
		try( Scanner sc = new Scanner(new File(path +  filename))){
			while(sc.hasNextLine()){
				currentLine++;
				currentLineContent = sc.nextLine();
				if(line == currentLine){
					return currentLineContent;				
				}
			}
			
		}catch( FileNotFoundException e){
			System.out.println(e.getMessage());
		}
					
		return null; 
	}

}
