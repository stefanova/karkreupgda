package pl.org.pfig.student.resolver.student;

import pl.org.pfig.student.connector.DatabaseConnector;
import pl.org.pfig.student.model.Student;
import pl.org.pfig.student.resolver.AbstractResolver;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class StudentResolver extends AbstractResolver <Student>{
  List<Student> listOfStudents = new ArrayList<Student>();


  public Student get(int id) {

      Student student = null;
      try {
        Connection connection = DatabaseConnector.getInstance().getConnection();
        String query = "SELECT student FROM `student` WHERE id =" + id;

        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery(query);
        if(rs.next()) {
          student = new Student(
                  rs.getInt("id"),
                  rs.getString("name"),
                  rs.getString("lastname"));
        }
        rs.close();
        s.close();
        connection.close();

      } catch ( SQLException e){
        System.out.println(e.getMessage());
      }

      return student;
    }


  public List get() {

//    try{
//      Connection connection = DatabaseConnector.getInstance().getConnection();
//      String querty = "Select * FROM" +
//
//    }
    return null;
  }

  public boolean delete(int id) {
    return false;
  }

  public boolean update(int id, Map params) {

//    try{
//      Connection connection = DatabaseConnector.getInstance().getConnection();
//      Statement s = connection.createStatement();
//      s.execute()
//    }
    return false;
  }

  public boolean insert(Map params) {
    return false;
  }
}
