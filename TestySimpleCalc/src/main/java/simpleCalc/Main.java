package simpleCalc;

import matrix.Matrix;

public class Main {
	

	public static void main(String[] args) {
		int[][] m = new int[4][4];
		Matrix mat = new Matrix();

		m = mat.identityMatrix(m);
		
		mat.print2DArray(mat.identityMatrix(m));
		mat.print2DArray(mat.indexedMatrix(m));

	}

	public int[][] identityMatrix(int[][] m) {
		if (m.length != m[0].length) {
			for (int i = 0; i < m.length; i++) {
				for (int j = 0; j < m[i].length; j++) {
					if (i == j) {
						m[i][j] = 1;
					}
				}
			}
		}

		return m;
	}


}