package simpleCalc;

import static org.junit.Assert.*;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class SimpleCalcAddMethodTest {
	
	private SimpleCalc sc;
	
	@Before
	public void doStuff(){
		
	sc = new SimpleCalc();	
	}
	
	@Test
	public void whenTwoPositiveNumbersAreGivenPositiveNumbersAreSum(){
		int a = 3, b = 6; 
		//sc.add(a, b);
		
		assertEquals(9, sc.add(a, b));
	}
	
	@Test
	public void whenFirstNegativeAndSecondPositiveNumbersAreSum(){
		int a = -4, b = 2; 		
		assertEquals(-2, sc.add(a, b)); 
	}
	
	@Test
	public void whenTwoNegativeNumbersAreSum(){
		int a = -4, b = -2; 
		
		assertEquals(-6, sc.add(a, b)); 
	}
	
	@Test
	public void whenFirstPositiveAndSecondNegativeNumbersAreSum(){
		int a = 4, b = -2; 
		assertEquals(2, sc.add(a, b)); 
	}

	@Test(expected=Exception.class)
	public void whenExTrhrowMethodIsUsedExceptionIsThrown() throws Exception{
		sc.exThrow();
	}
	
	@Test
	public void newDoubleTest(){
		double a = 3.1, b = 6.9; 
		assertEquals(10, sc.add(a, b), 0.01); 
	}
	
	@Test
	public void whenTwoMaxIntegersAreGivenPositiveNumbersAreExpected(){
		Assert.assertTrue("Out of range", sc.add(Integer.MAX_VALUE, Integer.MAX_VALUE) > 0);
	}
}
