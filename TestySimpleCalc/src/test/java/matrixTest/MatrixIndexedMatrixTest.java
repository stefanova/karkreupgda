package matrixTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import matrix.Matrix;

public class MatrixIndexedMatrixTest {
	
	Matrix m = new Matrix();
	
	@Before
	public void init(){
		m = new Matrix();
	}
	
	@Test
	public void whenEmptyArrayGivenIndexedMatrixTest(){
		int[][] actual = new int[2][2];
		int[][] expected = { {1,2}, {3,4}}; 
		assertArrayEquals(expected, m.indexedMatrix(actual));
				
	}
	
	@Test
	public void whenEmptyArrayGivenIndexedMatrixArrayDimensions(){
		int[][] actual = new int[2][2];
		int[][] expected = { {1,2}, {3,4} };
		
		for( int i = 0; i < actual.length; i++){
			assertTrue(actual[i].length == expected[i].length);
		}
		
	
	}
	
	@Test
	public void whenEmptyArrayGivenIndexedMatrixArrayDimensio(){
		
	}


}
