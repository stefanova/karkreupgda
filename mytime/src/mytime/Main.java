package mytime;
import mytime.*;


public class Main {
	public static void main(String[] args) {
		MyTime mt = new MyTime(22, 0, 65);
		MyTime mt2 = mt.nextHour();
		MyTime mt3 = mt.prevHour();
		MyTime mt4 = mt.nextSecond();
		MyTime mt5 = mt.nextMinute();
		MyTime mt6 = mt.prevMinute();
		
		System.out.println(mt);
		
		System.out.println(mt2);
		
		System.out.println(mt3);
		
		System.out.println(mt4);
		
		System.out.println(mt5);
		
		System.out.println(mt6);


	}
}
