package mytime;

public class MyTime {
	private int hour = 0;
	private int minute = 0;
	private int second = 0;
	
	
	public MyTime() { }
	
	public MyTime(int hour, int minute, int second) {
		setTime(hour, minute, second);
	}
	
	public void setTime(int hour, int minute, int second) {
		setHour(hour);
		setMinute(minute);
		setSecond(second);
	}

	@Override
	public String toString() {
		return leadZero(this.hour) + ":" 
			 + leadZero(this.minute) + ":" 
	  		 + leadZero(this.second);
	}
	
	private String leadZero(int num) {
		if(num < 10) {
			return "0" + num;
		} else {
			return "" + num; // dlaczego mamy "" ? - ze wzgl�du na to, aby zwr�ci� liczb� jako string, a nie int
		}
	}

	public int getHour() {
		return hour;
	}

	public void setHour(int hour) {
		if(hour >= 0 && hour <= 23) {
			this.hour = hour;
		}
	}

	public int getMinute() {
		return minute;
	}

	public void setMinute(int minute) {
		if(minute >= 0 && minute <= 59) {
			this.minute = minute;
		}
	}

	public int getSecond() {
		return second;
	}

	public void setSecond(int second) {
		if(second >= 0 && second <= 59) {
			this.second = second;
		}
	}
	
	public MyTime nextHour() {
		int newHour = hour + 1;
		if(newHour == 24) {
			newHour = 0;
		}
		return new MyTime(newHour, minute, second);
	}
	
	public MyTime nextSecond(){
		int newHour = hour;
		int newMinute = minute;
		int newSecond = second + 1;
		
		if (newSecond == 60){
			newSecond = 0;
			newMinute ++;
			
		} if (newMinute == 60) {
			newMinute = 0;
			newHour ++;
		} if (newHour > 23){
			newHour = 0;
		}
		
		return new MyTime(newHour, newMinute, newSecond); 
	} 
	
	public MyTime nextMinute(){
		int newHour = hour;
		int newMinute = minute + 1;
		int newSecond = second; 
		
		if (newMinute == 60) {
			newMinute = 0;
			newHour ++;
		} if (newHour > 23){
			newHour = 0;
		}
		
		return new MyTime(newHour, newMinute, newSecond);
	} 
	
	public MyTime prevHour(){
		int newHour = hour - 1;
		if(newHour == 0) {
			newHour = 23;
		}
		return new MyTime(newHour, minute, second);
	}
	

	public MyTime prevSecond(){
		int newHour = hour;
		int newMinute = minute;
		int newSecond = second - 1;
		
		if (second == 0){
			newSecond = 59;
			newMinute ++;
			
		} if (newMinute == 60) {
			newMinute = 0;
			newHour ++;
		} if (newHour > 23){
			newHour = 0;
		}
		
		return new MyTime(newHour, newMinute, newSecond); 
	} 
	
	public MyTime prevMinute(){
		int newHour = hour;
		int newMinute = minute - 1;
		int newSecond = second;
		
		if (minute == 0){
			newMinute = 59;
			newHour = hour - 1;
		}
		return new MyTime(newHour, newMinute, newSecond);

	}
	
	
}
	
