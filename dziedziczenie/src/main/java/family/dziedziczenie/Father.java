package family.dziedziczenie;

public class Father extends FamilyMember {
	
	public Father (String name){
	super(name);
}
	@Override
	public void introduce( ){
		System.out.println( "I'm a father. My name is: " + this.getName());

	}
}