package family.dziedziczenie;

public class Family {

	public static void main(String[] args) {

//		Mother mother = new Mother("Gra�yna"); - mozemy zamiast Mother, zrobic FamilyMember i bedzie to samo
		FamilyMember mother = new Mother("Gra�yna");
//		Father father = new Father("Stanis�aw");
		FamilyMember father = new Father("Stanis�aw");
		FamilyMember son = new Son("Adam");
		FamilyMember daughter = new Daughter("Jadzia");

		FamilyMember[] members = {mother, father, son, daughter};
		
		for(FamilyMember familyMember : members){
			familyMember.introduce();
		}
		
//		
// 		mother.introduce();
//		father.introduce();
//		son.introduce();
//		daughter.introduce();

	}
}
