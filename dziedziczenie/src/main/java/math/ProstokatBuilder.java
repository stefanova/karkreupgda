package math;

public class ProstokatBuilder {
	public static void main(String[] args) {

		generatedByBuilder();

	}

	private static void generatedByBuilder() {
		StringBuilder builder = new StringBuilder();

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				builder.append('*');

			}

			builder.append('\n');

			System.out.print(builder.toString());

		}
	}

}
