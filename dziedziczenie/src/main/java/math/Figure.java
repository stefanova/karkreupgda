package math;

public interface Figure {
	
	public static void main(String[] args) {
		
		Figure square = new Square(2);
		
		System.out.println("Pole kwadratu to: " + square.countArea() + 
				"Obwod kwadratu to:" + square.countCircumference());
		
		
	}
	
	double countArea();	
	double countCircumference();

}
