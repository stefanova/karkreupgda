package math;

import org.junit.Test;

public class FiguresTest {

	@Test
	public void FiguresTest() {

		Square square = new Square(2.0);
		assert square.countArea() == 4.0;
		
		Square squareObwod = new Square(4);
		assert squareObwod.countCircumference() == 16.0;

	}

}
