package math;

public class Circle implements Figure {
	
	private double r;
	
	public Circle(double r) {
		this.r = r;
	}

	@Override
	public double countArea(){
		return Math.PI*r*r ;
	}
	
	@Override
	public double countCircumference(){
		return 2 * countArea();
	}

}
