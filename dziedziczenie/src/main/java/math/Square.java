package math;

public class Square implements Figure {
	
	private double a;
			
	public Square(double a) {
		super();
		this.a = a;
	}

	@Override
	public double countArea(){
		return a*a ;
	}
	
	@Override
	public double countCircumference(){
		return 4 * a;
	}

}
