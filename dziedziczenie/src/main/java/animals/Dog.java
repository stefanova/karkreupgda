package animals;

public class Dog extends AnimalType {
	
	public Dog(String animalKind, String name, int age){
		
		super(animalKind, name, age );
	}
	
	@Override
	public void animalDetails(){
		System.out.println("This is a " + this.getName() +"." + " It is " + this.getAge() + " years old. ");
	}

}
