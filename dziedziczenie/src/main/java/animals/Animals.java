package animals;

//glowna klasa, w ktorej wywolujemy wszystkie metody i tworzymy obiekty

public class Animals {
	
	public static void main(String[] args) {
		
		AnimalType dog = new Dog("dog","Pimpu�", 5);
		AnimalType cat = new Cat("Mruczek", 8, 4);
		AnimalType cat2 = new Cat("Random", 3, 4);
		
		dog.animalDetails();
		cat.animalDetails();
		
		cat2.animalDetails();
		
	}

}
