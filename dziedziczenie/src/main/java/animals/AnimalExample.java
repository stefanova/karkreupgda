package animals;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class AnimalExample {

	public static void main(String[] args) {
		
		
		Animal[] animals = {new Bird(), new Turtle()};
		
		for(Animal animal : animals){
			System.out.println( animal.makeNoise() );
		}
		makeNoise5TimesToFile("noises.txt", new Bird());
		
	}
	
	public static void makeNoise5TimesToFile(String filename, Animal animal){
		try { 
			PrintStream out = new PrintStream (filename);
			for (int i = 0; i < 5; i++){
				out.println(animal.makeNoise());
			}
			out.close();
		} catch (FileNotFoundException e){
			e.printStackTrace();
		}
	
	}

}
