package animals;

public class Cat extends AnimalType{
	
	private int legs;
	
	public Cat(String name, int age, int legs) {
		super("cat",name, age);
		this.legs = legs;
	}
	
	@Override
	
	public void animalDetails(){
	System.out.println("This is a "+ this.getName() 
	+"." + " It is " + this.getAge() + " years old. " 
	+ " And has " + this.getLegs() + " legs" + this.getAnimalKind());
	}
	
	public int getLegs() {
		return legs;
	}

}
