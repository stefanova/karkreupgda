package animals;

//klasa tworzaca obiekty, ktore maja wspolne cechy

public abstract class AnimalType {
	
	private String animalKind;
	private String name;
	private int age;

	
	public AnimalType(String animalKind, String name, int age){
	
	this.animalKind = animalKind;
	this.name = name; 
	this.age = age;
}
		
	public String getName() {
		return name;
	}
	public int getAge() {
		return age;
	}
	
	public String getAnimalKind() {
		return animalKind;
	}
	
	public abstract void animalDetails();
}
