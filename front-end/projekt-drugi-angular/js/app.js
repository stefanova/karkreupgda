var app = angular.module('secondApp', []);

app.controller('temperatureController', ['$scope', function($scope){
                                                                             
        $scope.computeC = function(){
            $scope.fahr = ($scope.cel*1.8)+32;
            $scope.kel = 273.15 + $scope.cel * 1;
        }
         $scope.computeK = function(){
            $scope.fahr = $scope.kel * 1.8 -459.69;
            $scope.cel = $scope.kel-273.15;
        }
          $scope.computeF = function(){
            $scope.cel = ($scope.fahr -32) / 1.8;
            $scope.kel= ($scope.fahr *1 +459.67)*5/9;
        }
                                       }])