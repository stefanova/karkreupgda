


var countButton = document.getElementById('przeliczButton');



countButton.addEventListener('click', function(e){
    
    var amount = parseFloat(document.getElementById('przeliczInput').value);
    var opts = document.querySelectorAll('#currency > option');    
    var fromOpts = document.querySelectorAll('#fromCurrency > option');
    
    var selIndexFromOpts = document.querySelector('[name="currencySelectFrom"]').selectedIndex;
    var selIndex = document.querySelector('[name="currencySelect"]').selectedIndex;
    
    var dataRateValue = opts[selIndex].dataset.rate; 
    var dataFromCurrencyValue = fromOpts[selIndexFromOpts].dataset.rate;

    var result = amount * dataFromCurrencyValue / dataRateValue; 
    
//    var printedResult = document.createElement('div');
//    printedResult.innerHTML = result.toFixed(2) + " PLN";
    
    alert(result.toFixed(2) + " PLN");

});