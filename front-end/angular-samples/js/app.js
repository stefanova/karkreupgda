var app = angular.module('firstApp',[]);

app.controller('firstController', ['$scope','$filter', function($scope, $filter){
   $scope.message=" Wiadomość z kontrolera #1";
    $scope.user={
        name:"Karolina",
        lastname:$filter('limitTo')("Testowa", 3),
        salary: 1234.89
    };
}])

app.controller('secondController', function(){
    this.info = "Wiadomość z kontrolera #2";
})

app.controller('thirdController', ['$scope','$log', 'limitToFilter', 
    function($scope, $log, limitToFilter){
    $scope.message = limitToFilter("Wiadomość z kontrolera #3", 10);
    
    $scope.myList= [
        'pierwsza',
        'druga',
        'trzecia',
        'czwarta',
        'piata'
    ];
    
    $scope.showMessage= function(){
        alert($scope.message + ' ' + $scope.$parent.message);
    }
    
}] )

app.controller('fourthC',['$scope','$http', function($scope, $http){
     $scope.showCurrencies = function(){
            $http({
                url:'http://api.fixer.io/latest',
                method: 'GET',
                dataType: 'json'
                          }).then(function(res) {
                $scope.rates ="";
                var rates = res.data.rates;
                var keys = Object.keys(rates);
                var ul = document.createElement('ul');
                for(let key of keys){
                   var li = document.createElement('li');
                    li.innerHTML = key +" : " +rates[key];
                    ul.appendChild(li);
                }
                document.getElementById('result').appendChild(ul);
            }, function(err) {
                console.warn(err);
            });
         console.log('czesc jestem kar')
                          }                   
}]);

app.controller('fifthC',['$scope', '$http', function($scope, $http){
    $scope.showUsers= function(){
        $http({
            
            url: 'https://randomuser.me/api/',
            method: 'GET',
            dataType: 'json'}).then(function(res){
            $scope.results="";
            var users = res.data.results[0];
            var name = res.data.results[0].name;
            var keys = Object.keys(users);
            var location = res.data.results[0].location;
            var ul2 = document.createElement('ul');
            for(let key of keys){
                var li2 = document.createElement('li2');
                li2.innerHTML = key + " : " + name[key];
                ul2.appendChild(li2);
            }
         document.getElementById('result2').appendChild(ul2);
        }, function(err){
            console.warn(err);
        }
                                   
            )
    }
    
}])