package generic;

public interface SimpleQueue<T> {

  boolean isEmpty();
  void offer(T value);
  T poll();
  T peek();
}
