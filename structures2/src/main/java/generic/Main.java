package generic;

public class Main {

  public static void main(String[] args) {
    SimpleQueue<String> queue = new SimpleQueueArray<>();
    queue.offer("AA");
    queue.offer("BB");
    queue.offer("CC");

    System.out.println(queue.peek());

    while(!queue.isEmpty()){
      System.out.println(queue.poll());
    }
  }
}
