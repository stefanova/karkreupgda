package queue;

public interface SimpleQueue {

  boolean isEmpty();
  void offer(int value);
  int poll();
  int peek();

}
