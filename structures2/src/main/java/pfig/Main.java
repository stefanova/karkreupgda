package pfig;

public class Main {

    public static void main(String[] args) {
        SimpleList list = new SimpleArrayList();

        list.add(1);
        list.add(2);
        list.add(5);
        list.add(5, 1);

        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));

    }
}
