package pfig;

import org.assertj.core.api.Assertions;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class SimpleArrayListTest {

  private SimpleArrayList underList = new SimpleArrayList();

  @Test
  public void addToEmptyList() throws Exception {
    underList.add(4);

    assertThat(underList.contain(4)).isTrue();
    assertThat(underList.contain(10)).isFalse();
    assertThat(underList.get(0)).isEqualTo(4);
    assertThat(underList.size()).isEqualTo(1);
  }


  @Test
  public void addToNotEmptyList() throws Exception {
    underList.add(1);
    underList.add(2);

    underList.add(4);

    assertThat(underList.contain(4)).isTrue();
    assertThat(underList.contain(10)).isFalse();
    assertThat(underList.get(2)).isEqualTo(4);
    assertThat(underList.size()).isEqualTo(3);
  }

  @Test
  public void addInTheMiddleOfTheList() throws Exception {
    underList.add(4);
    underList.add(8);

    underList.add(2,1);

    assertThat(underList.contain(2)).isTrue();
    assertThat(underList.contain(4)).isTrue();
    assertThat(underList.contain(8)).isTrue();

    assertThat(underList.get(1)).isEqualTo(2);
    assertThat(underList.get(2)).isEqualTo(8);
    assertThat(underList.size()).isEqualTo(3);
  }

  @Test(expected = IndexOutOfBoundsException.class)
  public void addWithTooBigIndex() throws Exception {
    underList.add(2);
    underList.add(4);

    underList.add(20,6);

  }
  @Test
  public void removeIfExist() throws Exception {
    underList.add(1);
    underList.add(2);
    underList.add(8);

    underList.removeValue(2);

    assertThat(underList.contain(1)).isTrue();
    assertThat(underList.contain(8)).isTrue();
    assertThat(underList.contain(2)).isFalse();
  }

  @Test
  public void removeIfNotExist() throws Exception {
    underList.add(1);
    underList.add(8);

    underList.removeValue(2);

    assertThat(underList.contain(1)).isTrue();
    assertThat(underList.contain(8)).isTrue();
    assertThat(underList.contain(2)).isFalse();
  }

  @Test
  public void removeIndex() throws Exception {
    underList.add(1);
    underList.add(5);

    underList.remove(1);

    assertThat(underList.size()).isEqualTo(1);
    assertThat(underList.contain(5)).isFalse();

  }


  @Test(expected = IndexOutOfBoundsException.class)
  public void removeIndexOutOfBound() throws Exception {
    underList.add(1);
    underList.add(5);

    underList.remove(8);
  }

  @Test
  public void size() throws Exception {

  }

}