package pl.org.pfig.main;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
    	
    	StringUtil su = new StringUtil("przykladowy tekst");
    	StringUtil su2 = new StringUtil ("kolejny tekst");
    	StringUtil su3 = new StringUtil ("przyklad");
    	
    			
    	su.print().prepend("____").print();
    	su2.print().append("____").print();
    	su3.print().letterSpacing().print();
    	
    	
    	System.exit(0);
        
        
        Random r = new Random();
        // je�eli nie przekazujemy parametru, mo�e zosta� zwr�cona loczba ujemna
        System.out.println(r.nextInt());
        // je�eli przekazujemy argument, zostanie zwr�cona liczba z zakresu <0, liczba>
        System.out.println(r.nextInt(15));
        
        // generowanie losowej liczby z zakresu
        // <10,25> 25 - 10 = 15
        int a = 10;
        int b = 25;
        
        System.out.println(a+r.nextInt(b-a));
        
        char gen = (char)(a+r.nextInt(b-a));
        System.out.println(gen + "");
        
        HTMLExercise he = new HTMLExercise("To jest moj tekst.");
        
        he.print().strong().print().p().print();
        
        String myStr = "  ciag znakow  ";

        if (myStr.equals("  ciag znakow  ")) {
            System.out.println("ciagi sa taki same.");
        }

        if (myStr.equalsIgnoreCase("  CIAG znakow  ")) {
            System.out.println("Ciag znak�w jest taki sam, bez badania wielko�ci liter.");
        }

        System.out.println("D�ugo�� myStr to: " + myStr.length());

        System.out.println(myStr.substring(10));

        /*
         * M I C H A L 0 1 2 3 4 5
         */

        System.out.println(myStr.substring(0, 6));

        System.out.println(myStr.substring(1, myStr.length() - 4));

        System.out.println(myStr.trim()); // usuwa "bia�e" znaki (spacje)

        System.out.println(myStr.charAt(5));

        System.out.println(myStr.charAt(5) + "" + myStr.charAt(8)); // dwa chary
                                                                    // bez
                                                                    // Stringa
                                                                    // przekonwertuj�
                                                                    // si� na
                                                                    // int

        String alphabet = "";
        for (char c = 97; c <= 122; c++) {
            alphabet += c;
        }
        
        System.out.println(myStr.toUpperCase());
        System.out.println(myStr.toLowerCase());
        System.out.println(alphabet);

        System.out.println(myStr.replace("ciag", "lancuch"));

        String otherStr = "kamien w wode";

        System.out.println(myStr.concat(otherStr));

        if (myStr.contains("iag")) {
            System.out.println("slowo iag zawiera sie w myStr" + myStr);
        }
        
        if (alphabet.startsWith("a")) {
            System.out.println("alphabet zaczyna si� na a");
        }
        
        if (alphabet.endsWith("z")) {
            System.out.println("alphabet konczy sie na z");
        }
        
        System.out.println(myStr.indexOf("a"));
        System.out.println(myStr.lastIndexOf("a"));
        
        String simpleStr = "pawel poszedl do lasu";
        
        String[] arrOfStr = simpleStr.split(" ");
        for (String s: arrOfStr) {
            System.out.println("\t" + s);
        }
        
        
        
        String word = "onomatopeja";
        System.out.println(word.charAt(0)+word.substring(1).replace(word.charAt(0), '_'));
        
    }
        
        
        
        
    
        public static String[] checkWordLength(int n, String s) {
        
        String[] words = s.split(" ");
        String[] ret = new String[words.length];
        int i = 0;
        for (String word: words) {
            if (word.length() > n){
                ret[i++] = word;
            }
        }
        return ret;
        
        
        }
        
        
    
        
}