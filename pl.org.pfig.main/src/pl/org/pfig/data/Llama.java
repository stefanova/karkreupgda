package pl.org.pfig.data;

public class Llama implements AnimalInterface, Soundable {
	
	private String name;
	
	public Llama(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		
		return this.name;
	}

	@Override
	public String getSound() {
		return "Lam, lam";
	}

}
