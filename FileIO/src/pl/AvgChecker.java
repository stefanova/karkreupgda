package pl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.Scanner;

public class AvgChecker {
	
	
	private final String path = "resources/";
	private String filename;
	
	public AvgChecker( String filename){
		this.filename = filename;
	}
	
	public void process(){
		
		//1. Odczytac wszystki linie z pliku
		File f = new File(path + filename);
		double avg = 0; 
		
		LinkedList<String> fileContent = new LinkedList<>(); 
		
		
		try {
			Scanner sc = new Scanner(f);
			String currentLine;
						
			while(sc.hasNextLine()){
				currentLine = sc.nextLine();			
					if (!currentLine.equals(" "));{
					fileContent.add(currentLine);
					avg += countAvgFromLine(currentLine);
				
				}
			}
						
			avg /= fileContent.size();
			sc.close();
			
			System.out.println("�rednia srednich " + avg);
			
			FileOutputStream fos = new FileOutputStream(f);
			PrintWriter pw = new PrintWriter(fos);
			
			for(String line : fileContent){
				if(countAvgFromLine(line) > avg){
					pw.println(line);
				}
			}
			
			pw.close();
			
		} catch (FileNotFoundException e) {
			
			System.out.println(e.getMessage());
		}}
		
		private double countAvgFromLine(String line){
			String[] marks = line.split("\t");
			
			int marksSum = 0;
			
			// prasujemy na double poszczegolne wartosci

			for (int i = 1; i < marks.length; i++){
				marksSum += Double.parseDouble(marks[i]);
			}
			
			return marksSum / (marks.length -1);
		}
	}				


