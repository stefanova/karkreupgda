package pl;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Columner {
	
	private final String path = ("resources/");
	
	private String filename;
	private double[] currentColumn; 
	
	private LinkedList<String> fileContent = new LinkedList<>();
	
	public Columner(){
		
	}
	
	public Columner (String filename){
		this.filename = filename; 
		
	}
	
	private void readFile(){
		if(fileContent.size() ==0){
			File f = new File(path + filename);
			String currentLine = null;
		
		try {
			Scanner sc = new Scanner(f);
			while(sc.hasNextLine()){
				fileContent.add(currentLine);
			}
			sc.close();
			
		} catch (FileNotFoundException e) {
			System.out.println("Exception occured"); 
	
	}
			

}}
		
		public void readColumn(int column){
			readFile();
			int maxColNum = fileContent.get(0).split("\t").length;
			
			if(column >= 1 && column <= maxColNum){
				currentColumn = new double[fileContent.size()];
				
				int i = 0; 
				
				for(String line : fileContent){
					String[]  cols = line.split("\t");
							
					currentColumn[i++] = Double.parseDouble(cols[column -1]);
				}
			}
		}
		
	}
