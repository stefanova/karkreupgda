package pl.org.pfig.factoryOther;

public class CouponFacotry {

    private CouponFacotry(){};

    public static Coupon getCoupon(int [] numbers){

        //Mozna zapisać tak:
//        Coupon c = new Coupon(numbers[0], numbers[1],
//                numbers[2], numbers[3], numbers[4], numbers[5]);

        //Albo tak:
        return new Coupon(
                    numbers[0],
                    numbers[1],
                    numbers[2],
                    numbers[3],
                    numbers[4],
                    numbers[5]
                        );
    }
}
