package pl.org.pfig.decoratorPatternPerson;

import java.io.FileNotFoundException;
import java.io.PrintStream;

public class FilePrinter {
    private final PersonPrinter personPrinter;

    public FilePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    public void print( Person person, String filename){
        try(PrintStream printToFile = new PrintStream(filename)) {
            personPrinter.print(person, printToFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
    }

    }
}