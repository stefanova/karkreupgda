package pl.org.pfig.decoratorPatternPerson;

import java.io.PrintStream;

public class AgePrinter implements PersonPrinter {

    public AgePrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    private PersonPrinter personPrinter;

    @Override
    public void print(Person person, PrintStream out) {

        personPrinter.print(person,out);
        out.println("age : " + person.getAge());

    }
}
