package pl.org.pfig.decoratorPatternPerson;

public class PrintStreamTest {

    private final static String FILE_NAME = "person.txt";

    public static void main(String[] args) {


        PersonPrinter personPrinter = new AgePrinter(
                                new HeightPrinter(
                                new EyesColourPrinter(
                                new BasicDataPrinter()
                        )
                        ));

        Person person = new PersonBuilder().withName("Jan").withSurname("Ble").withAge(20).withWeight(81.5).withHeight(175).withEyesColor(Person.EyesColor.BLUE).createPerson();
        personPrinter.print(person, System.out);

        FilePrinter printer = new FilePrinter(personPrinter);
        printer.print(person, "newFile.txt");

    }
}
