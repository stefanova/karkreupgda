package pl.org.pfig.decoratorPatternPerson;

import java.io.PrintStream;

public class EyesColourPrinter implements PersonPrinter{

    private PersonPrinter personPrinter;

    public EyesColourPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }

    @Override

    public void print(Person person, PrintStream out) {

        personPrinter.print(person,out);
        out.println("eyes colour is: " + person.getEyesColor());

    }
}
