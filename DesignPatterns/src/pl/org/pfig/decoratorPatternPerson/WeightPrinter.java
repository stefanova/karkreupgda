package pl.org.pfig.decoratorPatternPerson;

import java.io.PrintStream;

public class WeightPrinter implements PersonPrinter{
    private PersonPrinter personPrinter;

    public WeightPrinter(PersonPrinter personPrinter) {
        this.personPrinter = personPrinter;
    }


    @Override
    public void print(Person person, PrintStream out) {

        personPrinter.print(person,out);

        out.println("weight : " + person.getWeight());
    }
}
