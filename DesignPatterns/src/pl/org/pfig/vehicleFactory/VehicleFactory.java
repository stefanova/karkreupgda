package pl.org.pfig.vehicleFactory;

public class VehicleFactory {

    public static Vehicle getVehicle(String vehicle){
        switch (vehicle){
            case "car":
                return new Vehicle("car");
            case "truck":
                return new Vehicle("track");
            case "bike":
                return new Vehicle("motor bike");
        }
        throw new IllegalArgumentException("Unknown animal.");
    }
}
