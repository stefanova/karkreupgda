package pl.org.pfig.vehicleFactory;

public class Vehicle {
    private String type;

    public String getType() {
        return type;
    }

    public Vehicle(String type) {

        this.type = type;
    }
}
