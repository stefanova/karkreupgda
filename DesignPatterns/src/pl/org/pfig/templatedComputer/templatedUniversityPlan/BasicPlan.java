package pl.org.pfig.templatedComputer.templatedUniversityPlan;

public abstract class BasicPlan {

    public void basicSubject(){
        philosophy();
        pe();
        art();
        specialization();
        System.out.println();
    }

    public void philosophy(){
        System.out.println("Filozofia ");
    }
    public void pe(){
        System.out.println("WF");
    }

    public void art(){
        System.out.println("Plastyka");
    }

    public abstract void specialization();
}
