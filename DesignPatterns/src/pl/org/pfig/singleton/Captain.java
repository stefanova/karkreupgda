package pl.org.pfig.singleton;

public class Captain {

    private static Captain _instance;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    private Captain(){};

    public static Captain getInstance(){
        if(_instance == null){
            System.out.println("Stanowisko kapitana jest wolne. Musimy wybrac jakiegos ochotnika!");
            _instance = new Captain();
            System.out.println("Kapitan zostal wybrany jednoglosnie. Gratulacje!");
        }
        System.out.println("Pokaż się oficjalnie wybrany Panie kapitanie :D");

        System.out.println("Dzien dobry, tu kapitan Kazimierz. Klaniam sie nisko i pozdrawiam rodzine!");
        return _instance;

    }

}
