package pl.org.pfig.factoryPattern;

public class AnimalFactory {

    public static AnimalInterface getAnimal(String animal){
        animal = animal.toLowerCase();
        switch (animal){
            case "dog":
                return new Dog();
            case "cat":
                return new Cat();
            case "frog":
                return new Frog();
        }
        throw new IllegalArgumentException("Unknown animal.");
    }
}
