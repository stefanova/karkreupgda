package pl.org.pfig.factoryPattern;

public class Dog implements AnimalInterface{
    @Override
    public void getSound() {
        System.out.println("Hau hau!");
    }
}
