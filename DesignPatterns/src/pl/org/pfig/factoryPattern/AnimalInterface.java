package pl.org.pfig.factoryPattern;

public interface AnimalInterface {

    public void getSound();

}
