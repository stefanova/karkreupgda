package pl.org.pfig.factoryPattern;

public class Cat implements AnimalInterface {

    @Override
    public void getSound() {
        System.out.println("miau, miau!");
    }
}
