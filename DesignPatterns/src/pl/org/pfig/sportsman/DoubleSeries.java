package pl.org.pfig.sportsman;

public class DoubleSeries implements Sportsman {

    private Sportsman sportsman;

    public DoubleSeries(Sportsman sportsman) {
        this.sportsman = sportsman;

    }


    @Override
    public void prepare() {

        sportsman.prepare();

    }

    @Override
    public void doPumps(int pumps) {

        pumps *= 2;
        sportsman.doPumps(pumps);


    }

    @Override
    public void doSquats(int squats) {

        squats *= squats;
        sportsman.doSquats(squats);

    }

    @Override
    public void doCrunches(int crunches) {

        crunches *= 2;
        sportsman.doCrunches(crunches);

    }
}