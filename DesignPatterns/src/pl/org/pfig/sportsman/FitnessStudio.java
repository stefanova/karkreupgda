package pl.org.pfig.sportsman;

public class FitnessStudio {

    public void train(Sportsman sportsman){
        sportsman.prepare();
        sportsman.doPumps(1);
        sportsman.doSquats(2);
        sportsman.doCrunches(5);
    }

}
