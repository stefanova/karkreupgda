package pl.org.pfig;

import pl.org.pfig.PersonFluidInterface.People;
import pl.org.pfig.PersonFluidInterface.Person;
import pl.org.pfig.sportsman.*;
import pl.org.pfig.vehicleFactory.Vehicle;
import pl.org.pfig.vehicleFactory.VehicleFactory;

import java.util.LinkedList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        //Singleton Pattenr ************************************************
//        SingletonExample se1 = SingletonExample.getInstance();
//        SingletonExample se2 = SingletonExample.getInstance();

//        Captain kazimierz = Captain.getInstance();
//        Captain romuald = Captain.getInstance();
//        kazimierz.setName("Kazimierz Jędrzej");
//        System.out.println(romuald.getName());

        //Template method ************************************************

//        Laptop laptop = new Laptop();
//        System.out.println("Laptop: ");
//        laptop.devices();
//
//        MidiTowerComputer mtc = new MidiTowerComputer();
//        System.out.println("MidiTowerComputer: ");
//        mtc.devices();
//
//        PersonalComputer pc = new PersonalComputer();
//        System.out.println("PersonalComputer: ");
//        pc.devices();

//        Math matematyka = new Math();
//        System.out.println("Matematyka przedmioty: ");
//        matematyka.basicSubject();
//
//        Sociology socjologia = new Sociology();
//        System.out.println("Socjologia przedmioty: ");
//        socjologia.basicSubject();

        // Factory pattern ************************************************

//        AnimalInterface cat = AnimalFactory.getAnimal("cat");
//        cat.getSound();
//
//        AnimalInterface dog = AnimalFactory.getAnimal("dog");
//        dog.getSound();
//
//        AnimalInterface frog = AnimalFactory.getAnimal("frog");
//        frog.getSound();
//        AnimalInterface tiger = AnimalFactory.getAnimal("tiger");
//        tiger.getSound();


        //Factory Pattern 2 **************************************************

//        int[] numbers = new int[6];
//        for(int i = 0; i <numbers.length; i++){
//            numbers[i] = new Random().nextInt(48) +1;
//        }
//
//        Coupon c = CouponFacotry.getCoupon(numbers);
//        System.out.println(c.getA());

        Vehicle vehicle = VehicleFactory.getVehicle("car");
        System.out.println( vehicle.getType());

        Vehicle vehicle2 = VehicleFactory.getVehicle("bike");
        System.out.println( vehicle2.getType());

//        FLUID INTERFACE **************************************************
//
//        List<Person> people = new LinkedList<>();
//        people.add(new Person("Pawel", "Testowy", 30));
//        people.add(new Person("Gawel", "Fredek", 24));
//        people.add(new Person("Marian", "Testowy", 44));
//        people.add(new Person("Pawel", "Fredek", 38));
//
//        People pp = new People().addGroup("staff", people);
//
//        for (Person pers: pp.from("staff").lastName("Testowy").name("Marian").get()){
//            System.out.println(pers);
//        };



        Sportsman sportsman = new WaterDrinking(new BasicSportsman());
        //Sportman sportman = new BasicSportman;

        //fluent interface - za pomoca buildera
        /* sportsman.Sportsman sportsman = sportsmanWho();
        .alwaysDoesDoubleSeries()
        .drinksWater()
        .makesSelfies()
        .getHim()
         */


        FitnessStudio fitnessStudio = new FitnessStudio();
        fitnessStudio.train(sportsman);

        Sportsman sportsman1 = (new DoubleSeries (new WaterDrinking(new BasicSportsman())));

    }
}
