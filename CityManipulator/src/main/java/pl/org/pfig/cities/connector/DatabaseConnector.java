package pl.org.pfig.cities.connector;

import java.sql.*;

public class DatabaseConnector {

  private static DatabaseConnector _instance = null;
  private Connection _connection;

  // Constructor
  private DatabaseConnector(DatabaseConnectorBuilder dcb) throws SQLException{
    String url = "jdbc:mysql://" + dcb.hostname() + ":" + dcb.port() + "/" + dcb.database();

    _connection = DriverManager.getConnection(url,dcb.username(),dcb.password());
  }


  // Constructor tworzacy instancje i przypisujacy jej wartosc.
  public static DatabaseConnector getInstance() throws SQLException{
    if(_instance == null){
      _instance = new DatabaseConnector( new DatabaseConnectorBuilder()
                                        .hostname("localhost")
                                        .port(3306)
                                        .username("root")
                                        .password("")
                                        .database("rental"));
    }
    return _instance;
  }

  public Connection getConnection(){
    return _connection;
  }
}
