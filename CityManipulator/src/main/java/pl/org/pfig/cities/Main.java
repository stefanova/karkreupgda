package pl.org.pfig.cities;

import pl.org.pfig.cities.connector.DatabaseConnector;
import pl.org.pfig.cities.model.City;

import javax.swing.plaf.nimbus.State;
import javax.xml.crypto.Data;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

  public static void main(String[] args) {



    try {
      Connection con = DatabaseConnector.getInstance().getConnection();
      Statement s = con.createStatement();
      s.execute( "UPDATE `city` SET `city` = 'Gdańsk' WHERE city = 'Gdansk'");

      s.close();

    } catch (SQLException e){
      System.out.println(e.getMessage());
    }

    try{
      Connection con = DatabaseConnector.getInstance().getConnection();
      Statement s = con.createStatement();

      s.execute("DELETE FROM `city` WHERE city = 'Sieradz'");
    } catch (
            SQLException e){
      System.out.println(e.getMessage());
    }

    try {
      Connection c = DatabaseConnector.getInstance().getConnection();

      Statement s = c.createStatement();
      s.execute("INSERT INTO `city` VALUES (NULL, 'Sieradz')");

      s.close();

    }
    catch(SQLException e){
      System.out.println(e.getMessage());
    }

    try {
              Connection c = DatabaseConnector.getInstance().getConnection();
              String query = "UPDATE `city` SET city = ? WHERE `city` = ?";
              PreparedStatement ps = c.prepareStatement(query);
              ps.setString(2, "Poznan");
              ps.setString(1, "Kościerzyna");
              int countRows = ps.executeUpdate();
      System.out.println( "Napisalismy" + countRows + "wiersz(y).");
      ps.close();
    } catch (SQLException e ){
      System.out.println(e.getMessage());
    }



      // select
    List<City> listOfCities = new ArrayList<>();

    try {
      Connection connection = DatabaseConnector.getInstance().getConnection();
      String query ="SELECT * FROM `city`";

      Statement s = connection.createStatement();
      ResultSet rs = s.executeQuery(query);
      while (rs.next()){
        listOfCities.add(new City(rs.getInt("id"), rs.getString("city")));
      }
      rs.close();
      s.close();
      connection.close();
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }

    for(City c : listOfCities){
      System.out.println("--> " + c.getCity());
    }

  }

}
