var app = angular.module('RESTApp', ['ngRoute']);
var url = 'http://localhost:8080/';

app.config(function($routeProvider) {
    var path = './views/';
    $routeProvider
        .when('/', {
            templateUrl: path + 'main.html'
        })
        .when('/add', {
            templateUrl: path + 'add.html',
            controller: 'addController'
        })
        .when('/show', {
            templateUrl: path + 'animals.html',
            controller: 'showController'
        })
        .when('/show/:id', {
            templateUrl: path + 'animal.html',
            controller: 'animalController'
        })
        .when('/species', {
            templateUrl: path + 'species.html',
            controller: 'speciesController'
        })
        .when('/specie/:id', {
            templateUrl: path + 'specie.html',
            controller: 'specieController'
        });
});

app.controller('addController', function($scope, $http) {
    $scope.add = function() {
        $http({
            url: url + 'animals/add',
            method: 'GET',
            dataType: 'json',
            params: {
                name: $scope.animalName,
                description: $scope.desc,
                image: $scope.image
            }
        }).then(function(success) {
            console.log(success);
            $scope.message = "Dodano poprawnie zwierzątko.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('showController', function($scope, $http) {
    $http({
        url: url + 'animals/show',
        dataType: 'json'
    }).then(function(success) {
        $scope.animals = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('animalController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'animals/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.animal = success.data;
    }, function(error) {
        console.error(error);
    });
});

app.controller('speciesController', function($scope, $http) {
    
    $http({
        url: url + 'species/show'
    }).then(function(success) {
        $scope.species = success.data;
    }, function(error) {
        console.error(error);
    });
    
    
    $scope.add = function() {
        $http({
            url: url + 'species/add',
            dataType: 'json',
            params: {
                name: $scope.name,
                description: $scope.description
            }
        }).then(function(success) {
            if(success.data.id > 0) {
                $scope.species.push(success.data);
                $scope.message = "Gatunek dodano poprawnie."
            } else
                $scope.message = "Wystąpił błąd podczas dodawania gatunku.";
        }, function(error) {
            console.error(error);
        });
    }
});

app.controller('specieController', function($scope, $http, $routeParams) {
    var id = $routeParams.id;
    $http({
        url: url + 'species/show/' + id,
        dataType: 'json'
    }).then(function(success) {
        $scope.specie = success.data;
    }, function(error) {
        console.error(error);
    });
});




















