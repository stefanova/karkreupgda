
public class RepoName {
	
	
	public RepoName(String name, String secondName) {
		this.name = name;
		this.secondName = secondName;
	}

	private String name;
	private String secondName;
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getSecondName() {
		return secondName;
	}
	
	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}
	

	public static String getRepoName(String name, String secondName) {
        if (name.toLowerCase().charAt(2) == secondName.toLowerCase().charAt(0)) {
            return name.substring(0, 1).toUpperCase() 
                    + name.substring(1, 4).toLowerCase() 
                    + secondName.substring(0, 1).toUpperCase()
                    + secondName.substring(1, 4).toLowerCase() 
                    + "UPgda";
        } else {
            return name.substring(0, 1).toUpperCase() 
                    + name.substring(1, 3).toLowerCase() 
                    + secondName.substring(0, 1).toUpperCase()
                    + secondName.substring(1, 3).toLowerCase() 
                    + "UPgda";
        }
    }

    public String getRepoName() {
        return getRepoName(name, secondName);            // DRY - Don't Repeat Yourself
    }
	
}

