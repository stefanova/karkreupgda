package exceptions;

public class Division {
	
	public static double divide (int a, int b) throws IllegalArgumentException{
		if( b == 0){
			throw new IllegalArgumentException("Podaj liczby wi�ksze od 0");
		}
		
		return a / b;
	}
	
	public static double divide (double a, double b) throws IllegalArgumentException{
		if ( b == 0){
			throw new IllegalArgumentException("Double --> Podaj liczby wi�ksze od 0");
		}
		
		return a / b;
	}

}
