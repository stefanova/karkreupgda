package exceptions;

public class ExceptionSimpleClass {

	public void make(int a) throws Exception, IllegalArgumentException {
		
		if(a == 55){
			throw new IllegalArgumentException("Niepoprawny argument");
		}

		if (a == 5) {
			throw new Exception("a jest r�wne 5!");
		} else {
			System.out.println("a jest poprawn� warto�ci�");
		}
	}
	public void exceptionExample(String name) throws PabException{
		if(name.equalsIgnoreCase("pawel")){
			throw new PabException("Jestes pawlem!");
		}

}
}