package exceptions;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ReadNumbers {

	public static double readDouble() throws InputMismatchException {
		
		double in;
		Scanner sc = new Scanner(System.in);
		System.out.println("Podaj liczbe typu double.");
		
		try{
			in = sc.nextDouble();
		}catch(InputMismatchException e){
			in = 0;
		}
		
		return in;
	}

}
