package exceptions;

public class Square {
	
	
	public static double square(int n) throws IllegalArgumentException {
		if(n < 0) {
			throw new IllegalArgumentException("Zly argument");
		}
		return Math.sqrt(n);
	}

}

