package pl.org.pfig.tdd;

public class Main {

	public static void main(String[] args) {
		determineGCD(48, 12);
	}
	
	public static int determineGCD(int a, int b){
		
		// liczba a jest dzielna 
		//liczba b jest dzielnikiem
		// gcd jest najwiekszym wspolnym dzielnikiem
		
		// trzeba podzielic a przez b tyle razy, zeby wynik koncowy dawal nam a == b
		
		while(a != b){
			while( a > b){
				a = a - b;
			}
			while(b > a){
				b = b - a; 
			}
		}
		System.out.println(a);
		    return a;           
	}

}
