package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineMothTest {
	
	Exercises e; 
	
	@Before
	
	public void init(){
		e = new Exercises(); 
	}
	
	@Test
	
	public void whenProperValueIsGivenProperMonthIsExpected(){
		
		int[] arg = {1, 12};
		String[] expected = {"styczen", "grudzien"};
		
		for(int i = 0; i < arg.length; i++){
			assertEquals(expected[i], e.determineMonth(arg[i])); 
		}
	}
	
	@Test
	public void whenMiddleValueIsGivenProperMonthIsExpected(){
		
		int arg = 6;
		String expected = "czerwiec"; 
		
		assertEquals(expected, e.determineMonth(arg));
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void whenOutOfRangeValueIsGivenExceptionIsExpected(){
		int arg = 13; 
		e.determineMonth(arg);
	}

}
