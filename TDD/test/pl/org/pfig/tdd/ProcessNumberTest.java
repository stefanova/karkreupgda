package pl.org.pfig.tdd;

import org.junit.Assert.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProcessNumberTest {
	
	Exercises e; 
	
	@Before
	
	public void init(){
		e = new Exercises(); 
	}
	
	@Test
	public void whenArgumentAisOutOfRangeExceptionExpected(){
		//{a, b}
		IllegalArgumentException ex = null; 
		
		int arrayOfaA[] = {-1, 256, 1000, -1000};
		int b = 8; 
		
		for(int a : arrayOfaA){
		try{
			e.processNumber(a,b);		
		} catch ( IllegalArgumentException e){
			ex = e; 
		}
		assertTrue(ex != null);
		ex = null; 
		
	}}
	
	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentBisOutOfRangeExceptionExpected(){
		
		IllegalArgumentException ex = null; 
		
		int arrayOfaB[] = {-123, 256, 1000, -1000};
		int a = 3; 
		
		for(int b : arrayOfaB){
		try{
			e.processNumber(a,b);		
		} catch ( IllegalArgumentException e){
			ex = e; 
		}
		assertNotNull("Wyjątek nie wystąpił :(", ex);
		ex = null; 
		
	}
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void whenArgumentsAreInIncorrectOrderExceptionExpected(){
		//{a, b}
		
		int a = 8, b = 3; 
		e.processNumber(a, b);
	}
	
	@Test
	public void whenArgumentsAreProperExpectArrayResult(){
		int a = 3, b = 8; 
		int[] expected = {4, 6 ,8, 7, 5, 3};
		assertArrayEquals(expected, e.processNumber(a, b));
	
	}
	
	@Test
	public void whenArgumentsHaveEqualValueExpectOneItemArray(){
		int a = 5, b = 5; 
		int[] expected = {5};
		
		assertArrayEquals(expected, e.processNumber(a, b));
	}
	
	
}
