package pl.org.pfig.tdd;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineDayOfWeekTest {
	
	Exercises e; 
	
	@Before
	
	public void init(){
		e = new Exercises();
	}
	
	@Test
	public void whenProperValueIsGivenProperDayIsExpected(){
		int[] arg = {1,7}; 
		String[] expected = {"ndz", "sob"};
		
		for (int i = 0; i < arg.length; i++){
			assertEquals(expected[i], e.determineDayOfWeek(arg[i]));
		}		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void whenOutOfRangePositiveArgumentIsGivenExceptionExpected(){
		int arg = 99;
		e.determineDayOfWeek(arg);
	}
	
	@Test
	
	public void whenOutOfRangeNumberIsGiven(){
		int arg = -9; 
		IllegalArgumentException iae = null; 
		
		try{
			e.determineDayOfWeek(arg);
		}catch(IllegalArgumentException e){
			iae = e; 
		}
		
		assertNotNull("Wyjątek nie wystąpił", iae);
	}
		
		@Test
		
		public void whenPropperMediumValueIsGivenProperDayIsExpected(){
			int arg = 4; 
			String expected = "sr";
			assertEquals(expected, e.determineDayOfWeek(arg));
			
		}
		
	
	

}
