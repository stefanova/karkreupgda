package pl.org.pfig.tdd;

import org.junit.Assert.*;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class DetermineGCDTest {
	
	Exercises e; 
	
	@Before
	
	public void init(){
		e = new Exercises(); 
	}
	
	@Test
	
	public void whenProperValueIsGivenProperGCDisExpected(){
		
		int[][] dataSet = {{48, 12, 12},
						{-9, 3, 3},
						{21, -7, 6},
						{-36, -6, 6}};
		
		for(int[] data : dataSet){	
		assertEquals(data[2], e.determineGCD(data[0], data[1]));
		}		
	}
	
	@Test(expected = IllegalArgumentException.class)
	
	public void whenZeroIsGivenAsDividerExceptionIsExpected(){
		int a = 12; 
		int b = 0; 
		e.determineGCD(a,b);
		

}}