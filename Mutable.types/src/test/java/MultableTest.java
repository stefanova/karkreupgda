import org.junit.Test;

public class MultableTest {

	@Test
	public void test() {
		MultableTest a = new MultableTest(10);
		MultableTest b = a;
		
		a.setValue(100);
		
		assert a.getValue() == 100;
		assert b.getValue() == 100;
		
		a.increment();

		assert a.getValue() == 101;
		assert b.getValue() == 101;
	}
}