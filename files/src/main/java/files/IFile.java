package files;

import java.util.List;

public interface IFile {

	void save(List<Student> studentsList);

	List<Student> load();

}