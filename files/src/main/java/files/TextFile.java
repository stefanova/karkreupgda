package files;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TextFile implements IFile {


	private final static String FILE_NAME = "src/main/resources/students_text.txt";
	
	
	/* (non-Javadoc)
	 * @see files.IFile#save(java.util.List)
	 */
	@Override
	public void save(List<Student> studentsList) {
		
		try{
			PrintStream printStream = new PrintStream(FILE_NAME);
			for(Student student : studentsList){
				printStream.println(student);
			}
			printStream.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see files.IFile#load()
	 */
	@Override
	public List<Student> load() {
		
		List<Student> students = new ArrayList<>();
		String currentLine;
			
		try (Scanner scanner = new Scanner( new BufferedInputStream(new FileInputStream(FILE_NAME)));) {
					
			while(scanner.hasNextLine()){
				currentLine = scanner.nextLine();
				String[] data = currentLine.split(" ");
				Student student = new Student(Integer.parseInt(data[0]), data[1], data[2]);
				students.add(student);
			} 
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return students;
	}
	
}
