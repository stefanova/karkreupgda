package files;

import java.util.HashMap;
import java.util.Map;

public class StudentMap {

	public static void main(String[] args) {

		Map<Integer,Student> students = new HashMap<>();
		
		students.put(100100, new Student(100100, "Jan", "Kowalski"));
		students.put(100101, new Student(100101, "Anna", "Kowalska"));
		students.put(100102, new Student(100102, "Damian", "Kwiatkowski"));
		
		for (Student student : students.values()) {
			System.out.println(student.getIndexNumber()+" : "+student.getName()+ " " + student.getSurname());
		}

		System.out.println(students.containsKey(100100));
		System.out.println(students.containsKey(100104));
		System.out.println(students.size());
		
	}

}