package files;

import java.util.ArrayList;
import java.util.List;

import javax.xml.soap.Node;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import xml.ParseXML;

public class XMLFile implements IFile {

	@Override
	public void save(List<Student> studentsList) {
		try {
			Document doc = ParseXML.newDocument();
			createContent(doc, studentsList);
			ParseXML.saveFile(doc, "students.xml");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void createContent(Document doc, List<Student> studentsList) {
		Element students = doc.createElement("students");
		doc.appendChild(students);

		for (Student student : studentsList) {
			Element studentElement = doc.createElement("student");
			
			students.setAttribute("index",String.valueOf(student.getIndexNumber()) );		
			students.appendChild(studentElement);
			
			
			Element nameElement = doc.createElement("name");
			nameElement.appendChild(doc.createTextNode(student.getName()));
			students.appendChild(nameElement);
			
			Element surnameElement = doc.createElement("surname");
			surnameElement.appendChild(doc.createTextNode(student.getSurname()));
			students.appendChild(surnameElement);

		}
	}

	@Override
	public List<Student> load() {
		
		List<Student> studentsList = new ArrayList<>();
		
		try{
	        Document doc = ParseXML.parseFile("students.xml");
	        NodeList students = doc.getElementsByTagName("student");
	        
	        for(int i = 0; i < students.getLength(); i++){
	        	Node studentNode = (Node) students.item(i);
	        	Student s = parse(studentNode);
	        	studentsList.add(s);
	        }

		} catch (Exception e){
			System.out.println("ble");
		}

		return null;
	}

   private Student parse(Node studentNode) {
				
            Element student = (Element) studentNode;

            int index = Integer.parseInt(student.getAttribute("index"));
            String name = ParseXML.getText(student, "name");
            String surname = ParseXML.getText(student, "surname");
            
            return new Student(index, name, surname);
            
			}
	

}
