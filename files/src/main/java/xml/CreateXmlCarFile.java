package xml;

import javax.xml.transform.TransformerFactoryConfigurationError;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateXmlCarFile {
	
	Document doc;
	
   public static void main(String argv[]) {

    new CreateXmlCarFile().create();
	   
   }

public  void create() throws TransformerFactoryConfigurationError {
	try {
         doc = ParseXML.newDocument();     
         crateContent();
         ParseXML.saveFile(doc, "cars.xml");
      } catch (Exception e) {
         e.printStackTrace();
      }
}

public void crateContent() {
	Element rootElement = createCars();
   
	 Element supercars = addSupercars(rootElement);
 
	 ParseXML.setAttribute(doc, supercars, "company", "Ferrari");
     
	 addCar(supercars, "formula one", "Ferrari 101");
	 addCar(supercars, "sports", "Ferrari 202");
}

public void addCar(Element supercars, String type, String name) {
	Element carname = doc.createElement("carname");
	 ParseXML.setAttribute(doc, carname, "type", type);
	 carname.appendChild(
	 doc.createTextNode(name));
	 supercars.appendChild(carname);
}


public Element addSupercars(Element rootElement) {
	Element supercar = doc.createElement("supercars");
	 rootElement.appendChild(supercar);
	return supercar;
}

public Element createCars() {
	Element rootElement = doc.createElement("cars");
	 doc.appendChild(rootElement);
	return rootElement;
}
}