package oop.osoba;

public class Osoba {
	
	//Stan
	private String imie;
	private int wiek;
	
	//constructor
	public Osoba(String imie, int wiek){
		this.imie = imie;
		this.wiek = wiek; 
	}
	
	//Zachowanie
	public void przedstawSie(){
		System.out.println( imie + "(" + wiek + ")");
	}
	
	public void setImie(String imie) {
		this.imie = imie;
	}
	
	public void setWiek(int wiek) {
		this.wiek = wiek;
	}
	
	public String getImie() {
		return imie;
	}

}
