package oop.osoba;

import java.util.Scanner;

public class OsobaMain {

	public static void main(String[] args) {
		Osoba jan = new Osoba("Jan", 23); 
		jan.przedstawSie();
//Referencja obiektu  //Tworzenie instancji obiektu
//Osoba po tej str jest
//obiektem, czyli mozna 
//napisac Osoba albo 
//objekt
	
		Osoba adam = new Osoba("Adam", 22); 
		adam.przedstawSie();
		adam.setImie("J�drek");
		
		System.out.println("Imi� Adama to " + adam.getImie());
		
	}
	
	

}
