package oop.teddybear;

import java.io.ObjectInputStream.GetField;

public class TeddyBear {
	
	private String name;
	private boolean tail;
	private int age;
	private boolean hasOwner;
	
	TeddyBear( String name, boolean tail, int age, boolean hasOwner){
		this.name = name;
		this.tail = tail;
		this.age = age;
		this.hasOwner = hasOwner;
		
	}
	
	public void showName(){
		System.out.println("Hello! I am " + name);
	}
	
	public int getAge() {
		return age;
	}
	
	public String getName() {
		return name;
	}
	
	public boolean getTail(){
		return tail;
	}
	


}
