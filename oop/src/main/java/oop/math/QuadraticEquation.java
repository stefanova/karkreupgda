package oop.math;

public class QuadraticEquation {
	
	private double a;
	private double b;
	private double c;
	
	public QuadraticEquation(double a, double b, double c) {
		super();
		this.a = a;
		this.b = b;
		this.c = c;
	}
	
	public double calcDelta(){
		return (b*b) - (4*a*c);
	}
	
	
	//b2- 4ac
	//-b - delta/2a
	

}
