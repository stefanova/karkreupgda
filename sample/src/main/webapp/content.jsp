<%@ page isELIgnored="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<section class="mainContent">
    <h2 class="anyTitle"> Strona z przepisami kuchennymi MniamMniam</h2>
    <p> PRZYGOTOWANIE
        Umyte golonki wkładamy do sporego garnka, dodajemy liście laurowe, ziele angielskie, pieprz, kostkę bulionową lub susz warzywny oraz sól, zalewamy wodą i gotujemy około 1,5 godziny. Po tym czasie dodajemy warzywa i gotujemy jeszcze około 45 minut. Ugotowaną golonkę ostudzamy. Wywar wykorzystujemy do dowolnej zupy.

        Przygotowujemy marynatę. Łączymy ciemne piwo z czosnkiem, majerankiem, kminkiem i słodką papryką. Skórkę na golonkach nacinamy w kratkę, golonki zalewamy marynatą, mięso dobrze nią nacieramy i odstawiamy na minimum 2 godziny. Golonki co jakiś czas obracamy w marynacie.
        Nagrzewamy piekarnik do 200 stopni. Golonki wyjmujemy z marynaty, układamy na blaszce lub w naczyniu żaroodpornym, obkładamy półplasterkami cebuli, polewamy marynatą i pieczemy około 40-45 minut.

        JAK PODAWAĆ
        Najlepiej smakuje podana z pieczonymi ziemniaczkami i zasmażaną kapustą.</p>

    <p>
        <form action="Servlet" method="POST">
        <p><label>Login: </label> <input type="text" name="login"/></p>
        <p><label>Hasło: </label> <input type="password" name="password"/></p>
        <p> <button type="submit">Wyślij</button> </p>
</form>

    </p>


</section>