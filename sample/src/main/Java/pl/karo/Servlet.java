package pl.karo;

import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

@WebServlet(name="Servlet")

public class Servlet extends HttpServlet {

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String name = request.getParameter("login");
    String pass = request.getParameter("password");
    List<String>colors = Arrays.asList("black", "yellow", "green");

    HashMap<String, String> users = new HashMap<String, String>();
    users.put("grzes", "testowy");
    users.put("pawel", "testowy123");
    users.put("karolina", "testowa");

    if(users.containsKey(name)){

      if(users.get(name).equals(pass)){
        request.setAttribute("colors", colors);
        request.setAttribute("name",name);
        request.getRequestDispatcher("about.jsp").forward(request,response);

      } else{
        request.setAttribute("message", "Złe hasło");
        request.getRequestDispatcher("error.jsp").forward(request,response);
      }
    } else{
      request.setAttribute("message", "Zły login i hasło");
      request.getRequestDispatcher("error.jsp").forward(request,response);
    }




  }

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

  }


}
