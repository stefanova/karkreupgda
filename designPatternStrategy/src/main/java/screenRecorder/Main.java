package screenRecorder;

import java.util.Scanner;

public class Main {

  public static void main(String[] args) {

    System.out.println(createProfile());
  }

  public static Profile createProfile(){
    Scanner sc = new Scanner(System.in);

    System.out.println( "Podaj typ kodeku");
    String codecType = sc.nextLine();

    System.out.println("Podaj rodzaj rozszerzenia pliku");
    String extensionType = sc.nextLine();

    System.out.println("Podaj rozdzielczość");
    String resolutionType = sc.nextLine();

    Codec codec = Codec.valueOf(codecType);
    Resolution resolution = Resolution.valueOf(resolutionType);
    Extension extension = Extension.valueOf(extensionType);
    sc.close();

    return new Profile(codec, extension, resolution);

  }



}
