package screenRecorder;

public class Profile {

  private Resolution profileResolution;
  private Codec profileCodec;

  public Resolution getProfileResolution() {
    return profileResolution;
  }

  public void setProfileResolution(Resolution profileResolution) {
    this.profileResolution = profileResolution;
  }

  public Extension getProfileExtension() {
    return profileExtension;
  }

  public void setProfileExtension(Extension profileExtension) {
    this.profileExtension = profileExtension;
  }

  private Extension profileExtension;

  public Codec getProfileCodec() {
    return profileCodec;
  }

  public void setProfileCodec(Codec profileCodec) {
    this.profileCodec = profileCodec;
  }


  public Profile(Codec newCodec, Extension newExtension, Resolution newResolution) {
    this.profileExtension = newExtension;
    this.profileCodec = newCodec;
    this.profileResolution = newResolution;

  }

  public Profile(){

  }

  @Override
  public String toString() {
    return "Profile{" +
            "profileResolution=" + profileResolution +
            ", profileCodec=" + profileCodec +
            ", profileExtension=" + profileExtension +
            '}';
  }
}
