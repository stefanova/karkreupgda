package screenRecorder;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ScreenRecorder {


    private List<Profile> profileList = new ArrayList<Profile>();
    private Profile defaultProfile = new Profile();
    private boolean isRecording = false;


    //dodaje profil do listy
   public Profile addProfile(Profile newProfile){
     profileList.add(newProfile);

    return newProfile;
   }

      //ustawia profil (pole) na wybrany z listy
   public void setProfile(int profileNumber){

     defaultProfile = profileList.get(profileNumber);
   }

      //wylistowuje profile
    public void listProfiles(){
     for(int i = 0; i < profileList.size(); i++){
       System.out.println(i+1 + profileList.get(i).getProfileCodec().toString());
       System.out.println(i+1 + profileList.get(i).getProfileExtension().toString());
       System.out.println(i+1 + profileList.get(i).getProfileResolution().toString());

     }
    };

    //rozpoczyna nagrywanie (tylko gdy jeszcze nie nagrywamy)
    public void startRecording(){
      if(isRecording ){
        isRecording = true;
      }

    };

     //zatrzymuje nagrywanie (tylko gdy już nagrywamy)
   public void stopRecording(){
    if(isRecording == true){
      isRecording = false;
    }
   };
}
